#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk
import datetime

from Modelos.Model_Turno import Baja_Turno
from Modelos.Model_Turno import Consulta_Turno

from Controladores.Utilidades import Acomodar_Fecha

class Eliminar_Turno():

    def borrar_turno(self,widget):
        Baja_Turno(self.id_turn)
        today=datetime.datetime.now() #fecha actual
        dia,mes,anio=today.day,today.month-1,today.year
        self.cal.select_month(mes,anio)
        self.cal.select_day(dia)
        self.window.destroy()

    def delete_event(self,widget,event):
        self.window.destroy()

    def cerrar(self,widget):
        self.window.destroy()

    def __init__(self,Values_from_main):

        #Values_from_main contiene id del turno y un gtk calendar(de Core.py )
        archivo="Vistas/Turnos/eliminar_turno.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)

        self.id_turn=Values_from_main[0]
        self.cal=Values_from_main[1]

        self.window=glade.get_object("dialog1")
        self.button_aceptar=glade.get_object("button_aceptar")
        self.button_cancelar=glade.get_object("button_cancelar")
        self.entry_hora=glade.get_object("entry_hora")
        self.entry_fecha=glade.get_object("entry_fecha")
        self.entry_medico=glade.get_object("entry_medico")
        self.entry_paciente=glade.get_object("entry_paciente")
        self.textbuffer=glade.get_object("textbuffer1")
        value=Consulta_Turno(self.id_turn)

        self.entry_hora.set_text( value.getHora() )
        self.entry_fecha.set_text( Acomodar_Fecha(value.getFecha()) )
        self.entry_medico.set_text( value.getMedico() )
        self.entry_paciente.set_text( value.getPaciente() )
        self.textbuffer.set_text( value.getCausa() )

        self.window.connect("delete_event",self.delete_event)
        self.button_cancelar.connect("clicked",self.cerrar)
        self.button_aceptar.connect("clicked",self.borrar_turno)
