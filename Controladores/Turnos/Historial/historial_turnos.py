#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk

from Modelos.Model_Turno import Busqueda_Turnos
from Modelos.Model_Medico import Todos_Medicos
from Modelos.Model_Paciente import Todos_Pacientes

from Controladores.Utilidades import Acomodar_Fecha

class Historial_Turnos():


    def mostrar_historial(self,widget):

        busqueda=widget.get_text()
        self.liststore_historial.clear()
        if busqueda != "":
            lista=Busqueda_Turnos(busqueda,self.tipo)
            for Turn in lista:
                self.liststore_historial.append([ Turn.getHora(),
                                                Acomodar_Fecha(Turn.getFecha()),
                                                Turn.getPaciente(),
                                                Turn.getMedico(),
                                                Turn.getCausa(),
                                                Turn.getEstado() ] )

    def cambio_tipo(self,widget):
        self.liststore_historial.clear()
        self.entry_buscar_paciente.set_text("")
        self.entry_buscar_medico.set_text("")
        if widget.get_active_text() == "Paciente":
            self.notebook.set_current_page(0)
            self.tipo="PAC"
        else:
            self.notebook.set_current_page(1)
            self.tipo="MED"

    def delete_event(self,widget,event):
        self.window.destroy()

    def cerrar(self,widget):
        self.window.destroy()

    def __init__(self):

        archivo="Vistas/Turnos/historial_turnos.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)

        self.window=glade.get_object("dialog1")
        self.button_cerrar=glade.get_object("button_cerrar")
        self.entry_buscar_paciente=glade.get_object("entry1")
        self.entry_buscar_medico=glade.get_object("entry2")
        self.liststore_historial=glade.get_object("liststore1")

        self.entrycompletionPAC=glade.get_object("entrycompletion1")
        self.liststore_completationPAC=glade.get_object("liststore_completation")

        self.entrycompletionMED=glade.get_object("entrycompletion2")
        self.liststore_completationMED=glade.get_object("liststore_completation1")

        self.comboboxtipo=glade.get_object("combobox4")
        self.notebook=glade.get_object("notebook1")
        self.labeltipo=glade.get_object("label3")

        self.entrycompletionPAC.set_text_column(0)
        self.entrycompletionMED.set_text_column(0)
        self.entry_buscar_paciente.set_completion(self.entrycompletionPAC)
        self.entry_buscar_medico.set_completion(self.entrycompletionMED)

        self.cargar_lista_pacientes()
        self.cargar_lista_medicos()

        #por defecto busca pacientes
        self.tipo="PAC"

        self.entry_buscar_paciente.connect("changed",self.mostrar_historial)
        self.entry_buscar_medico.connect("changed",self.mostrar_historial)

        self.comboboxtipo.connect("changed",self.cambio_tipo)

        self.button_cerrar.connect("clicked",self.cerrar)
        self.window.connect("delete_event",self.delete_event)

    def cargar_lista_pacientes(self):
        """cargo todos los nomyape de pacientes en un gtk liststore
        utilizado en un entrycompletion"""
        lista=Todos_Pacientes()
        for Pac in lista:
            self.liststore_completationPAC.append([Pac.getNomyApe()])
    def cargar_lista_medicos(self):
        """cargo todos los nomyape de medicos en un gtk liststore
        utilizado en un entrycompletion"""
        lista=Todos_Medicos()
        for Med in lista:
            self.liststore_completationMED.append([Med.getNomyApe()])
