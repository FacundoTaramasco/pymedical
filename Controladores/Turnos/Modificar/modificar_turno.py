#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk
import datetime

from Modelos.Model_Turno import Turno
from Modelos.Model_Turno import Turnos_de_Mes
from Modelos.Model_Turno import Consulta_Turno
from Modelos.Model_Turno import Modificacion_Turno
from Modelos.Model_Turno import Turnos_Especificos

from Modelos.Model_Medico import Todos_Medicos
from Modelos.Model_Medico import Consulta_Medico
from Modelos.Model_Medico import ConsultaEspecificaMED

from Modelos.Model_Paciente import Todos_Pacientes
from Modelos.Model_Paciente import ConsultaEspecificaPAC

from Controladores.Utilidades import Acomodar_Fecha
from Controladores.Utilidades import Acomodar_dia_mes
from Controladores.Utilidades import Lista_Horarios
from Controladores.Utilidades import Cantidad_dia_mes
from Controladores.Utilidades import FechaADiaSemana
from Controladores.Utilidades import Fecha_Para_Calendar

from Controladores.Utilidades import Cargar_treeview_turnos
from Controladores.Utilidades import Limpiar_treeview_turnos

class Mensaje_modificaciones():

    def actualizar_datos(self,self_mod):
        #tomo los datos del turno a modificar
        pac=self_mod.entry_paciente.get_text()
        id_pac=ConsultaEspecificaPAC("id","nomyape",pac)
        med=self_mod.combobox_medicos.get_active_text()
        id_med=ConsultaEspecificaMED("id","nomyape",med)
        hora=self_mod.combobox_hora.get_active_text()
        fecha=Acomodar_Fecha(self_mod.entry_fecha.get_text() )
        start,end=self_mod.textbuffer.get_bounds()
        causa=self_mod.textbuffer.get_text(start,end)
        #seteo turno
        Tur=Turno()
        Tur.setId( self_mod.id_turn )
        Tur.setIdPac(id_pac)
        Tur.setIdMed(id_med)
        Tur.setHora(hora)
        Tur.setFecha(fecha)
        Tur.setCausa(causa)
        #modifico turno en la bd
        Modificacion_Turno(Tur)

    def realizar_tarea(self,widget,self_mod,error):
        if error:
            self.window.destroy()
        else:
            self.actualizar_datos(self_mod)
            today=datetime.datetime.now() #fecha actual
            dia,mes,anio=today.day,today.month-1,today.year
            self_mod.cal.select_month(mes,anio)
            self_mod.cal.select_day(dia)
            self_mod.window.destroy()
            self.window.destroy()

    def cerrar(self,widget):
        self.window.destroy()

    def delete_event(self,widget,event):
        self.window.destroy()

    def __init__(self,self_mod,error):
        archivo="Vistas/Turnos/mensaje_modificaciones.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)
        self.window=glade.get_object("Mensaje")
        self.button_aceptar=glade.get_object("button_aceptar")
        self.button_cancelar=glade.get_object("button_cancelar")
        label=glade.get_object("label1")
        if error:
            label.set_text("Valores incorrectos")
        else:
            label.set_text("¿Desea modificar el turno?")
        self.window.connect("delete_event",self.delete_event)
        self.button_cancelar.connect("clicked",self.cerrar)
        self.button_aceptar.connect("clicked",self.realizar_tarea,self_mod,error)


class Modificar_Turno():

    def modificar(self,widget):
        """Al realizar click en aceptar si llama a la clase
        Mensaje_modificaciones,si existen errores se informan,sino se
        actualiza el turno"""
        if self.turno_existente or self.error_paciente:
            Mensaje_modificaciones(self,error=True)
        else:
            Mensaje_modificaciones(self,error=False)

    def verificar_hora_fecha_y_medico(self,widget):
        #posiciono gtk notebook a la primera pagina
        self.notebook.set_current_page(0)
        #quito todas las marcas del gtk calendar
        self.calendar.clear_marks()
        #tomo la fecha seleccionada en el gtk calendar(en int)
        anio,mes,dia = self.calendar.get_date()
        #dia de la semana : lunes, martes, etc
        dia_semana = FechaADiaSemana(dia,mes,anio)
        #conventir a str
        anio=str(anio)
        dia,mes=Acomodar_dia_mes(dia,mes) # 1 --> 01 ... 2 --> 02 etc.
        #seteo fecha en label formato dd/mm/aaaa
        self.entry_fecha.set_text(dia+"-"+mes+"-"+anio)
        #tomo el medico actual
        medico=self.combobox_medicos.get_active_text()
        #tomo la hora actual
        hora=self.combobox_hora.get_active_text()
        #fecha formato aaaa/mm/dd
        fecha=Acomodar_Fecha(self.entry_fecha.get_text())
        #obtengo la id del medico actual
        id_med=ConsultaEspecificaMED("id","nomyape",medico)
        #realizo una consulta del medico
        Med=Consulta_Medico(id_med)
        #verifico si el medico trabaja el dia elejido de la semana
        #si no trabaja lo informo
        if dia_semana == "lunes" and not Med.getTrabajaLunes():
            self.notebook.set_current_page(1)
            self.label_mensaje.set_text(medico+" no trabaja los lunes.")
        if dia_semana == "martes" and not Med.getTrabajaMartes():
            self.notebook.set_current_page(1)
            self.label_mensaje.set_text(medico+" no trabaja los mastes.")
        if dia_semana == "miercoles" and not Med.getTrabajaMiercoles():
            self.notebook.set_current_page(1)
            self.label_mensaje.set_text(medico+" no trabaja los miercoles.")
        if dia_semana == "jueves" and not Med.getTrabajaJueves():
            self.notebook.set_current_page(1)
            self.label_mensaje.set_text(medico+" no trabaja los jueves.")
        if dia_semana == "viernes" and not Med.getTrabajaViernes():
            self.notebook.set_current_page(1)
            self.label_mensaje.set_text(medico+" no trabaja los viernes.")
        if dia_semana == "sabado" and not Med.getTrabajaSabado():
            self.notebook.set_current_page(1)
            self.label_mensaje.set_text(medico+" no trabaja los sabados.")
        if dia_semana == "domingo" and not Med.getTrabajaDomingo():
            self.notebook.set_current_page(1)
            self.label_mensaje.set_text(medico+" no trabaja los domingos.")
        #realizo marcas en el gtk calendar de todos los turnos del mes
        self.marcas_en_calendar(anio,mes,id_med)
        #setear treeview
        Limpiar_treeview_turnos(self.liststore_turnos)
        #cargarlo con los turnos de la fecha y medico actual
        Cargar_treeview_turnos(medico,fecha,self.liststore_turnos)
        Tur=Turno()
        Tur.setIdMed(id_med)
        Tur.setFecha(fecha)
        #tomo todos los turnos de la fecha y medico actual
        #para validar la modificacion del turno
        lista=Turnos_Especificos(Tur)
        self.turno_existente=False
        self.imagen_combobox.set_from_stock(gtk.STOCK_OK,1)
        self.imagen_combobox.set_tooltip_text("")
        for turn in lista:
            #si el medico del turno de la lista es igual al medico original
            #y si la fecha del turno de la lista es igual a la fecha original
            if (turn.getMedico() == self.medico_elejido) and \
            (turn.getFecha() == Acomodar_Fecha(self.fecha_guardada)):
                #si la hora elejida es distinta a la hora guardada(la original)
                #y si la hora elejida es igual a la hora del turno de la lista
                if (hora != self.hora_guardada) and (hora == turn.getHora()):
                    self.imagen_combobox.set_tooltip_text("Ya existe un turno"\
                     " en la misma hora.")
                    self.imagen_combobox.set_from_stock(gtk.STOCK_DIALOG_ERROR,1)
                    self.turno_existente=True
                    break
            else:
                #si el medico elejido o la fecha elejida son distitas a las
                #originales pero coincide con un turno
                if turn.getHora() == hora:
                    self.imagen_combobox.set_tooltip_text("Ya existe un turno"\
                     " en la misma hora.")
                    self.imagen_combobox.set_from_stock(gtk.STOCK_DIALOG_ERROR,1)
                    self.turno_existente=True
                    break

    def validar_paciente(self,widget):
        ingreso=self.entry_paciente.get_text()
        id_pac=ConsultaEspecificaPAC("id","nomyape",ingreso)
        #si existe el paciente
        if id_pac != None:
                self.entry_paciente.set_icon_from_stock(1,gtk.STOCK_OK)
                self.entry_paciente.set_property("secondary-icon-tooltip-text",
                                                None)
                self.error_paciente=False
        else:
            self.entry_paciente.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_paciente.set_property("secondary-icon-tooltip-text",
                                                "Paciente inexistente.")
            self.error_paciente=True

    def delete_event(self,widget,event):
        self.window.destroy()

    def cerrar(self,widget):
        self.window.destroy()

    def __init__(self,Values_from_main):
        #Values_from_main contiene id del turno y un gtk calendar(de Core.py )
        self.id_turn=Values_from_main[0]
        self.cal=Values_from_main[1]

        self.turno_existente=False
        self.error_paciente=False
        archivo="Vistas/Turnos/modificar_turno.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)

        self.window=glade.get_object("dialog1")
        self.button_cancelar=glade.get_object("button_cancelar")
        self.button_aceptar=glade.get_object("button_aceptar")

        self.notebook=glade.get_object("notebook1")
        self.label_mensaje=glade.get_object("label12")

        self.combobox_hora=glade.get_object("combobox1")
        self.imagen_combobox=glade.get_object("image1")
        self.liststore_hora=glade.get_object("liststore_hora")
        self.liststore_medicos=glade.get_object("liststore_medicos")
        self.calendar=glade.get_object("calendar1")

        self.entry_fecha=glade.get_object("entry_fecha")
        self.combobox_medicos=glade.get_object("combobox_medicos")
        self.entry_paciente=glade.get_object("entry_paciente")
        self.textbuffer=glade.get_object("textbuffer1")
        textbufferori=glade.get_object("textbuffer2")

        self.liststore_turnos=glade.get_object("liststore_turnos")
        self.liststore_pacientes=glade.get_object("liststore_pacientes")

        entry_hora_ori=glade.get_object("entry_hora_ori")
        entry_medico_ori=glade.get_object("entry_medico_ori")
        entry_fecha_ori=glade.get_object("entry_fecha_ori")
        self.entry_paciente_orig=glade.get_object("entry_paciente_orig")

        self.completion=glade.get_object("entrycompletion1")

        self.entry_paciente.set_completion(self.completion)
        self.completion.set_text_column(0)

        #cargar todos los pacientes en el entry completion
        self.cargar_lista_pacientes()

        #cargo los horarios en el liststore y en el gtk combobox
        for x,hora in enumerate(Lista_Horarios()):
            self.liststore_hora.append([hora] )
            self.liststore_turnos.append([1,hora,"","",False,x%2] )

        self.cell_color(glade)

        #a partir del turno seleccionado realizo una consulta
        value=Consulta_Turno(self.id_turn)

        self.hora_guardada=value.getHora()
        self.fecha_guardada=Acomodar_Fecha(value.getFecha() )
        self.medico_elejido=value.getMedico()

        #setear combobox en la posicion de la hora seleccionada
        self.combobox_hora.set_active(self.posicionar_hora(value.getHora()))
        #agregar medicos en el gtk combobox
        self.agregar_medicos_combobox()
        #setear entry y textbuffer con datos del turno
        self.entry_fecha.set_text(Acomodar_Fecha(value.getFecha()))
        self.entry_paciente.set_text(value.getPaciente())
        self.textbuffer.set_text(value.getCausa())
        textbufferori.set_text(value.getCausa())

        #de la fecha en string convierto a fecha en int
        dia,mes,anio=Fecha_Para_Calendar(self.fecha_guardada)
        #para posicionar en el gtk calendar
        self.calendar.select_month(mes,anio)
        self.calendar.select_day(dia)
        #marca en el gtk calendar los dias que existen turnos
        #convierto a str
        anio=str(anio)
        # 1 --> 01 ... 2 --> 02 etc.
        dia,mes=Acomodar_dia_mes(dia,mes)
        self.marcas_en_calendar(anio,mes,ConsultaEspecificaMED("id","nomyape",
                                self.medico_elejido) )

        #cargar treeview con todos los turnos del dia y medico especifico
        Cargar_treeview_turnos(value.getMedico(),value.getFecha(),
                            self.liststore_turnos)

        #setear entry con datos originales
        entry_hora_ori.set_text(value.getHora())
        entry_medico_ori.set_text(value.getMedico())
        entry_fecha_ori.set_text(Acomodar_Fecha(value.getFecha() ) )
        self.entry_paciente_orig.set_text(value.getPaciente() )

        self.button_aceptar.connect("clicked",self.modificar)
        self.button_cancelar.connect("clicked",self.cerrar)

        self.window.connect("delete_event",self.delete_event)

        self.entry_paciente.connect("changed",self.validar_paciente)

        self.combobox_hora.connect("changed",self.verificar_hora_fecha_y_medico)
        self.calendar.connect("day-selected",self.verificar_hora_fecha_y_medico)
        self.combobox_medicos.connect("changed",self.verificar_hora_fecha_y_medico)

    def posicionar_hora(self,hora_establecida):
        """retorna la hora del turno para posicionar en el combobox"""
        for x,hora in enumerate( Lista_Horarios() ):
            if hora == hora_establecida:
                return x

    def agregar_medicos_combobox(self):
        """Agrego todos los medicos a un gtk combobox y seteo
        la posicion del medico perteneciente al turno"""
        lista=Todos_Medicos()
        for x,Med in enumerate(lista):
            self.liststore_medicos.append([Med.getNomyApe() ] )
            if Med.getNomyApe()  == self.medico_elejido:
                self.combobox_medicos.set_active(x)

    def cargar_lista_pacientes(self):
        """Cargo en un liststore todos los pacientes,utilizado
        en un gtk entrycompletion"""
        lista=Todos_Pacientes()
        for Pac in lista:
            self.liststore_pacientes.append([Pac.getNomyApe()])

    def marcas_en_calendar(self,anio,mes,id_med):
        """Marca en el gtk calendar los dias en que hay turnos"""
        findemes=Cantidad_dia_mes(int(anio),int(mes)-1)
        fecha_inicial=anio+"-"+mes+"-01"
        fecha_final=anio+"-"+mes+"-"+findemes
        dias_con_turno=Turnos_de_Mes(fecha_inicial,fecha_final,id_med)
        for d in dias_con_turno:
            self.calendar.mark_day(int(d))

    def cell_color(self,glade):
        """Le doy color de fondo a los cellrender"""
        celdas=("cellrenderertext2","cellrenderertext3","cellrenderertext6",
                "cellrenderertoggle1")
        columnas=("treeviewcolumn1","treeviewcolumn2","treeviewcolumn3",
                "treeviewcolumn4")
        for x,columna in enumerate(columnas):
            if columna == "treeviewcolumn4":#el cell cleckbutton
                glade.get_object(columna).set_attributes(glade.get_object(celdas[x]),
                                                            active=x+1,
                                                            cell_background_set=5)
            else:
                glade.get_object(columna).set_attributes(glade.get_object(celdas[x]),
                                                            text=x+1,
                                                            cell_background_set=5)
            glade.get_object(celdas[x]).set_property('cell-background','#E0E0E0')
