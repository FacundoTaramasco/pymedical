#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk
import datetime

from Modelos.Model_Turno import Turno
from Modelos.Model_Turno import Alta_Turno

from Modelos.Model_Paciente import Todos_Pacientes
from Modelos.Model_Paciente import ConsultaEspecificaPAC

from Modelos.Model_Medico import ConsultaEspecificaMED

from Controladores.Utilidades import Acomodar_Fecha

class Agregar_Turno():

    def agregar(self,widget):
        hora=self.entry_hora.get_text()
        fecha=Acomodar_Fecha(self.entry_fecha.get_text())
        medico=self.entry_medico.get_text()
        paciente=self.entry_paciente.get_text()
        start,end=self.textbuffer.get_bounds()
        causa=self.textbuffer.get_text(start,end)
        id_pac=ConsultaEspecificaPAC("id","nomyape",paciente)
        id_med=ConsultaEspecificaMED("id","nomyape",medico)
        Tur=Turno()
        Tur.setHora(hora)
        Tur.setFecha(fecha)
        Tur.setIdMed(id_med)
        Tur.setIdPac(id_pac)
        Tur.setCausa(causa)
        Tur.setEstado(False)
        Alta_Turno(Tur)

        today=datetime.datetime.now() #fecha actual
        dia,mes,anio=today.day,today.month-1,today.year
        self.cal.select_month(mes,anio)
        self.cal.select_day(dia)

        self.window.destroy()

    def validar_paciente(self,widget):
        ingreso=self.entry_paciente.get_text()
        for paciente in self.liststore_pacientes:
            if ingreso == paciente[0]:
                self.entry_paciente.set_icon_from_stock(1,gtk.STOCK_OK)
                self.entry_paciente.set_property("secondary-icon-tooltip-text",None)
                self.button_aceptar.set_sensitive(True)
                break
            else:
                self.button_aceptar.set_sensitive(False)
                self.entry_paciente.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
                self.entry_paciente.set_property("secondary-icon-tooltip-text",
                                                    "Paciente inexistente.")

    def delete_event(self,widget,event):
        self.window.destroy()

    def cerrar(self,widget):
        self.window.destroy()

    def __init__(self,Values_from_main):

        #Values_from_main : medico,fecha,la hora del turno y el gtk calendar(de Core.py)
        archivo="Vistas/Turnos/agregar_turno.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)

        self.cal=Values_from_main[3]

        self.window=glade.get_object("dialog1")
        self.button_aceptar=glade.get_object("button_aceptar")
        self.button_cancelar=glade.get_object("button_cancelar")
        self.entry_hora=glade.get_object("entry_hora")
        self.entry_fecha=glade.get_object("entry_fecha")
        self.entry_medico=glade.get_object("entry_medico")
        self.entry_paciente=glade.get_object("entry_paciente")
        self.textbuffer=glade.get_object("textbuffer1")
        self.liststore_pacientes=glade.get_object("liststore_pacientes")

        self.completion = glade.get_object("entrycompletion1")

        self.entry_paciente.set_completion(self.completion)
        self.completion.set_text_column(0)

        self.entry_medico.set_text(Values_from_main[0])
        self.entry_fecha.set_text(Values_from_main[1])
        self.entry_hora.set_text(Values_from_main[2])

        self.cargar_lista_pacientes()

        self.entry_paciente.connect("changed",self.validar_paciente)
        self.window.connect("delete_event",self.delete_event)
        self.button_aceptar.connect("clicked",self.agregar)
        self.button_cancelar.connect("clicked",self.cerrar)

    def cargar_lista_pacientes(self):
        lista=Todos_Pacientes()
        for Pac in lista:
            self.liststore_pacientes.append([ Pac.getNomyApe() ] )
