#! usr/bin/env python
# -*- coding : utf-8 -*-

import gtk
import datetime

from Modelos.Model_Turno import Limpiar_Turnos
from Modelos.Model_Turno import TodosTurnosPacMed
from Modelos.Model_Turno import Lista_turnos_seleccion

from Modelos.Model_Paciente import Todos_Pacientes
from Modelos.Model_Paciente import ConsultaEspecificaPAC

from Modelos.Model_Medico import Todos_Medicos
from Modelos.Model_Medico import ConsultaEspecificaMED

from Controladores.Utilidades import Acomodar_Fecha
from Controladores.Utilidades import Acomodar_dia_mes
from Controladores.Utilidades import Lista_Horarios
from Controladores.Utilidades import Fecha_Para_Calendar


class Limpieza_Turnos():

    def realizando_limpieza(self,widget,Values_from_main):
        medico=Values_from_main[0]
        fecha=Values_from_main[1]
        liststore_main=Values_from_main[2]
        calendar_main=Values_from_main[3]
        if self.radiobutton_todos.get_active():
            propiedad="todos"
            paciente=self.entry_paciente.get_text()
            Limpiar_Turnos(propiedad,paciente,None,None,None,None)
        elif self.radiobutton_seleccion.get_active():
            propiedad="seleccion"
            paciente=self.entry_paciente.get_text()
            hora_inicial=self.combobox_hora_inicial.get_active_text()
            hora_final=self.combobox_hora_final.get_active_text()
            fecha_inicial=Acomodar_Fecha( self.entry_fecha_inicial.get_text() )
            fecha_final=Acomodar_Fecha( self.entry_fecha_final.get_text() )
            Limpiar_Turnos(propiedad,paciente,hora_inicial,hora_final,fecha_inicial,fecha_final)

        #selecciono la fecha nuevamente en el gtk calendar principal
        dia,mes,anio=Fecha_Para_Calendar(fecha)
        calendar_main.select_month(mes,anio)
        calendar_main.select_day(dia)

        self.window.destroy()

    def desbloquear_todos(self,widget):
        self.liststore_turnos.clear()
        self.alignment_todos.set_sensitive(True)
        self.alignment_seleccion.set_sensitive(False)

    def desbloquear_seleccion(self,widget):
        self.liststore_turnos.clear()
        self.alignment_todos.set_sensitive(False)
        self.alignment_seleccion.set_sensitive(True)

    def cambia_inicial(self,widget):
        """
        Segun la hora inicial elejida actualiza la hora final.
        Para el caso de que no pueda elejir por ejemplo: inicial 10:00,final 09:00
        """
        self.liststore_final.clear()
        for x,hora in enumerate(Lista_Horarios() ):
            if x >= widget.get_active():
                self.liststore_final.append([hora])
        self.combobox_hora_final.set_active(0)

    def mostrar_lista(self,widget):
        self.liststore_turnos.clear()
        if self.combobox_tipo.get_active_text() == "Paciente":
            paciente=self.combobox_pacientes.get_active_text()
            id_value=ConsultaEspecificaPAC("id","nomyape",paciente)
            col="id_paciente"
        if self.combobox_tipo.get_active_text() == "Medico":
            medico=self.combobox_medicos.get_active_text()
            id_value=ConsultaEspecificaMED("id","nomyape",medico)
            col="id_medico"
        if self.radiobutton_seleccion.get_active():
            hora_inicial=self.combobox_hora_inicial.get_active_text()
            hora_final=self.combobox_hora_final.get_active_text()
            fecha_inicial=Acomodar_Fecha( self.entry_fecha_inicial.get_text() )
            fecha_final=Acomodar_Fecha( self.entry_fecha_final.get_text() )
            Turnos=Lista_turnos_seleccion(col,id_value,hora_inicial,hora_final,fecha_inicial,fecha_final)
            for Turn in Turnos:
                self.liststore_turnos.append([ Turn.getHora(),
                                    Acomodar_Fecha( Turn.getFecha() ),
                                    Turn.getPaciente(),
                                    Turn.getMedico(),
                                    Turn.getCausa(),
                                    Turn.getEstado() ] )
        if self.radiobutton_todos.get_active():
            lista=TodosTurnosPacMed(col,id_value)
            for tur in lista:
                self.liststore_turnos.append([ tur.getHora(),
                                    Acomodar_Fecha( tur.getFecha() ),
                                    tur.getPaciente(),
                                    tur.getMedico(),
                                    tur.getCausa(),
                                    tur.getEstado() ] )

    def cambio_tipo(self,widget):
        tipo=widget.get_active_text()
        if tipo == "Paciente":
            self.notebook.set_current_page(0)
        if tipo == "Medico":
            self.notebook.set_current_page(1)

    def dia_elejido(self,widget):
        anio,mes,dia = self.calendar.get_date()
        dia_entry,mes_entry=Acomodar_dia_mes(dia,mes)
        if self.viene =="inicial":
            dia_fin,mes_fin,anio_fin=Fecha_Para_Calendar(self.entry_fecha_final.get_text() )
             #chequeo si el dia inicial es menor o igual a la fecha final
            if anio < anio_fin:
                self.entry_fecha_inicial.set_text(dia_entry+"-"+mes_entry+"-"+str(anio)  )
            if anio == anio_fin and mes < mes_fin:
                self.entry_fecha_inicial.set_text(dia_entry+"-"+mes_entry+"-"+str(anio)  )
            if anio == anio_fin and mes == mes_fin and dia <= dia_fin:
                self.entry_fecha_inicial.set_text(dia_entry+"-"+mes_entry+"-"+str(anio)  )
        elif self.viene == "final":
            dia_ini,mes_ini,anio_ini=Fecha_Para_Calendar(self.entry_fecha_inicial.get_text() )
            #chequeo si el dia final es mayor o igual a la fecha inicial
            if anio >anio_ini:
                self.entry_fecha_final.set_text(dia_entry+"-"+mes_entry+"-"+str(anio)  )
            if anio == anio_ini and mes > mes_ini:
                self.entry_fecha_final.set_text(dia_entry+"-"+mes_entry+"-"+str(anio)  )
            if anio == anio_ini and mes == mes_ini and dia >= dia_ini:
                self.entry_fecha_final.set_text(dia_entry+"-"+mes_entry+"-"+str(anio)  )

    def mostrar_calendario(self,widget,icon_pos,event,data):
        self.vbox3.show()
        self.viene=data
        if data == "inicial":self.label_msj.set_text("Editando fecha inicial")
        if data == "final":self.label_msj.set_text("Editando fecha final")

    def ocultar_calendario(self,widget):
        self.vbox3.hide()

    def delete_event(self,widget,event):
        self.window.destroy()

    def quit(self,widget):
        self.window.destroy()

    def __init__(self,Values_from_main):
        archivo="Vistas/Turnos/limpieza_turnos.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)

        self.window=glade.get_object("dialog1")

        self.combobox_tipo=glade.get_object("combobox4")
        self.notebook=glade.get_object("notebook1")

        self.calendar=glade.get_object("calendar1")
        self.vbox3=glade.get_object("vbox3")
        self.button_ok=glade.get_object("button3")
        self.label_msj=glade.get_object("label12")

        self.combobox_pacientes=glade.get_object("combobox3")
        self.liststore_pacientes=glade.get_object("liststore_pacientes")

        self.combobox_medicos=glade.get_object("combobox5")
        self.liststore_medicos=glade.get_object("liststore_medicos")

        self.liststore_turnos=glade.get_object("liststore3")

        self.button_aceptar=glade.get_object("button1")
        self.button_aplicar=glade.get_object("button3")
        self.button_lista=glade.get_object("button_lista")
        self.button_salir=glade.get_object("button2")

        self.frame_todos=glade.get_object("frame2")
        self.radiobutton_todos=glade.get_object("radiobutton1")
        self.alignment_todos=glade.get_object("alignment2")

        self.frame_seleccion=glade.get_object("frame3")
        self.radiobutton_seleccion=glade.get_object("radiobutton2")
        self.alignment_seleccion=glade.get_object("alignment3")

        self.combobox_hora_inicial=glade.get_object("combobox1")
        self.entry_fecha_inicial=glade.get_object("entry2")

        self.combobox_hora_final=glade.get_object("combobox2")
        self.entry_fecha_final=glade.get_object("entry3")

        self.liststore_inicial=glade.get_object("liststore1")
        self.liststore_final=glade.get_object("liststore2")

        columna_toggle=glade.get_object("treeviewcolumn6")
        self.cellrenderer_toggle = gtk.CellRendererToggle()
        columna_toggle.pack_start(self.cellrenderer_toggle, True)
        columna_toggle.add_attribute(self.cellrenderer_toggle, "active", 5)

        lista=Todos_Pacientes()
        for Pac in lista:
            self.liststore_pacientes.append([ Pac.getNomyApe() ] )
        if lista:self.combobox_pacientes.set_active(0)

        lista=Todos_Medicos()
        for Med in lista:
            self.liststore_medicos.append([ Med.getNomyApe() ] )
        if lista:self.combobox_medicos.set_active(0)

        for hora in Lista_Horarios():
            self.liststore_inicial.append([hora])
            self.liststore_final.append([hora])

        self.combobox_hora_inicial.set_active(0)
        self.combobox_hora_final.set_active(0)

        today=datetime.datetime.now() #fecha actual

        dia,mes=Acomodar_dia_mes(today.day,today.month-1)
        self.entry_fecha_inicial.set_text(dia+"-"+mes+"-"+str(today.year) )
        self.entry_fecha_final.set_text(dia+"-"+mes+"-"+str(today.year) )

        self.desbloquear_todos(None)

        self.combobox_tipo.connect("changed",self.cambio_tipo)

        self.radiobutton_todos.connect("toggled",self.desbloquear_todos)
        self.radiobutton_seleccion.connect("toggled",self.desbloquear_seleccion)

        self.combobox_hora_inicial.connect("changed",self.cambia_inicial)

        self.calendar.connect("day-selected",self.dia_elejido)
        self.button_ok.connect("clicked",self.ocultar_calendario)

        self.entry_fecha_inicial.connect("icon-press",self.mostrar_calendario,"inicial")
        self.entry_fecha_final.connect("icon-press",self.mostrar_calendario,"final")

        self.button_aceptar.connect("clicked",self.realizando_limpieza,Values_from_main)

        self.button_lista.connect("clicked",self.mostrar_lista)

        self.window.connect("delete_event",self.delete_event)
        self.button_salir.connect("clicked",self.quit)
