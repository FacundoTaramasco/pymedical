#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk

from Controladores.Utilidades import es_int
from Controladores.Utilidades import caracteres_validos
from Controladores.Utilidades import caracteres_validos_telefono

from Modelos.Model_Medico import Medico
from Modelos.Model_Medico import Alta_Medico

from Modelos.Model_Medico import Todos_Medicos
from Modelos.Model_Medico import ConsultaEspecificaMED

class Nuevo_Medico():

    def agregar(self,widget):
        nomyape=self.entry_nomyape.get_text()
        dni=self.entry_dni.get_text()
        nromat=self.entry_nromat.get_text()
        telefono=self.entry_tel.get_text()
        domicilio=self.entry_dom.get_text()
        Med=Medico()
        Med.setNomyApe(nomyape)
        Med.setDni(dni)
        Med.setNroMat(nromat)
        Med.setTelefono(telefono)
        Med.setDomicilio(domicilio)
        Med.setTrabajaLunes(self.checkbutton_lunes.get_active() )
        Med.setTrabajaMartes(self.checkbutton_martes.get_active() )
        Med.setTrabajaMiercoles(self.checkbutton_miercoles.get_active() )
        Med.setTrabajaJueves(self.checkbutton_jueves.get_active() )
        Med.setTrabajaViernes(self.checkbutton_viernes.get_active() )
        Med.setTrabajaSabado(self.checkbutton_sabado.get_active() )
        Med.setTrabajaDomingo(self.checkbutton_domingo.get_active() )
        #Doy de alta el nuevo medico
        Alta_Medico(Med)
        #Cargo la lista del combobox con los medicos existentes (de Core.py)
        self.liststore_medicos_main.clear()
        lista=Todos_Medicos()
        for medic in lista:
            self.liststore_medicos_main.append( [medic.getNomyApe()] )
        self.combobox_medicos.set_active(0)
        #habilito el treeview llegado el caso de que este desactivado
        #(si no existieran medicos)
        self.treeview_main.set_sensitive(True)
        self.window.destroy()

    def validar_nomyape(self,widget):
        self.nomyape_ok=False
        nomyape=self.entry_nomyape.get_text()
        if caracteres_validos(nomyape) and nomyape != "":
            if ConsultaEspecificaMED("id","nomyape",nomyape) == None:
                self.entry_nomyape.set_icon_from_stock(1,gtk.STOCK_OK)
                self.entry_nomyape.set_property("secondary-icon-tooltip-text","")
                self.nomyape_ok=True
            else:
                self.entry_nomyape.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
                self.entry_nomyape.set_property("secondary-icon-tooltip-text",
                                                "Medico existente")
        else:
            self.entry_nomyape.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_nomyape.set_property("secondary-icon-tooltip-text",
                                            "Incorrecto")

    def validar_dni(self,widget):
        self.dni_ok=False
        dni=self.entry_dni.get_text()
        if es_int(dni) and dni != "":
            if ConsultaEspecificaMED("id","dni",dni) == None:
                self.entry_dni.set_icon_from_stock(1,gtk.STOCK_OK)
                self.entry_dni.set_property("secondary-icon-tooltip-text","")
                self.dni_ok=True
            else:
                self.entry_dni.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
                self.entry_dni.set_property("secondary-icon-tooltip-text",
                                            "DNI existente")
        else:
            self.entry_dni.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_dni.set_property("secondary-icon-tooltip-text",
                                        "Incorrecto")

    def validar_nromat(self,widget):
        self.nromat_ok=False
        nromat=self.entry_nromat.get_text()
        if es_int(nromat) and nromat != "":
            if ConsultaEspecificaMED("id","nromat",nromat) == None:
                self.entry_nromat.set_icon_from_stock(1,gtk.STOCK_OK)
                self.entry_nromat.set_property("secondary-icon-tooltip-text","")
                self.nromat_ok=True
            else:
                self.entry_nromat.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
                self.entry_nromat.set_property("secondary-icon-tooltip-text",
                                            "Matricula existente")
        else:
            self.entry_nromat.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_nromat.set_property("secondary-icon-tooltip-text",
                                            "Incorrecto")

    def validar_telefono(self,widget):
        self.tel_ok=False
        telefono=self.entry_tel.get_text()
        if caracteres_validos_telefono(telefono) and telefono != "":
            self.entry_tel.set_icon_from_stock(1,gtk.STOCK_OK)
            self.entry_tel.set_property("secondary-icon-tooltip-text","")
            self.tel_ok=True
        else:
            self.entry_tel.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_tel.set_property("secondary-icon-tooltip-text",
                                        "Incorrecto")

    def validar_domicilio(self,widget):
        self.dom_ok=False
        domicilio=self.entry_dom.get_text()
        if caracteres_validos(domicilio) and domicilio != "":
            self.entry_dom.set_icon_from_stock(1,gtk.STOCK_OK)
            self.entry_dom.set_property("secondary-icon-tooltip-text","")
            self.dom_ok=True
        else:
            self.entry_dom.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_dom.set_property("secondary-icon-tooltip-text",
                                        "Incorrecto")

    def desbloquear_boton_aceptar(self,widget):
        if self.nomyape_ok and self.dni_ok and  self.nromat_ok and\
        self.dom_ok and self.tel_ok:
            self.button_aceptar.set_sensitive(True)
        else:
            self.button_aceptar.set_sensitive(False)

    def delete_event(self,widget,event):
        self.window.destroy()

    def cerrar(self,widget):
        self.window.destroy()

    def __init__(self,Values_from_main):

        #Values_from_main contiene : gtk combobox (de medicos)
        #                            gtk liststore (de medicos)
        #                            gtk treeview (general)
        #(de Core.py)

        archivo="Vistas/Medicos/agregar_medico.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)

        self.combobox_medicos=Values_from_main[0]
        self.liststore_medicos_main=Values_from_main[1]
        self.treeview_main=Values_from_main[2]

        self.window=glade.get_object("dialog1")
        self.button_aceptar=glade.get_object("button_aceptar")
        self.button_cancelar=glade.get_object("button_cancelar")

        self.entry_nomyape=glade.get_object("entry_nomyape")
        self.entry_dni=glade.get_object("entry_dni")
        self.entry_nromat=glade.get_object("entry_nromat")
        self.entry_tel=glade.get_object("entry_tel")
        self.entry_dom=glade.get_object("entry_dom")

        self.checkbutton_lunes=glade.get_object("checkbutton1")
        self.checkbutton_martes=glade.get_object("checkbutton2")
        self.checkbutton_miercoles=glade.get_object("checkbutton3")
        self.checkbutton_jueves=glade.get_object("checkbutton4")
        self.checkbutton_viernes=glade.get_object("checkbutton5")
        self.checkbutton_sabado=glade.get_object("checkbutton6")
        self.checkbutton_domingo=glade.get_object("checkbutton7")

        (self.nomyape_ok,
        self.dni_ok,
        self.nromat_ok,
        self.dom_ok,
        self.tel_ok)=(False,False,False,False,False)


        self.entry_nomyape.connect("changed",self.validar_nomyape)
        self.entry_dni.connect("changed",self.validar_dni)
        self.entry_nromat.connect("changed",self.validar_nromat)
        self.entry_tel.connect("changed",self.validar_telefono)
        self.entry_dom.connect("changed",self.validar_domicilio)

        self.entry_nomyape.connect("changed",self.desbloquear_boton_aceptar)
        self.entry_dni.connect("changed",self.desbloquear_boton_aceptar)
        self.entry_nromat.connect("changed",self.desbloquear_boton_aceptar)
        self.entry_dom.connect("changed",self.desbloquear_boton_aceptar)
        self.entry_tel.connect("changed",self.desbloquear_boton_aceptar)

        self.button_aceptar.connect("clicked",self.agregar)
        self.button_cancelar.connect("clicked",self.cerrar)
        self.window.connect("delete_event",self.delete_event)
