#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk

from Modelos.Model_Medico import Baja_Medico
from Modelos.Model_Medico import Consulta_Medico
from Modelos.Model_Medico import Todos_Medicos

from Modelos.Model_Turno import TodosTurnosPacMed

from Controladores.Utilidades import Acomodar_Fecha

class Eliminar_Medico():

    def borrar_medico(self,widget):
        #Doy de baja al medico
        Baja_Medico(self.id_med)
        #Actualizo el liststore de editar-medicos.py
        self.liststore_med.clear()
        lista=Todos_Medicos()
        for Med in lista:
            self.liststore_med.append([ Med.getId(),
                                        Med.getNomyApe(),
                                        Med.getDni(),
                                        Med.getNroMat(),
                                        Med.getTelefono(),
                                        Med.getDomicilio() ])
        self.window.destroy()

    def delete_event(self,widget,event):
        self.window.destroy()

    def cerrar(self,widget):
        self.window.destroy()

    def __init__(self,Values_from_edit):
        #Values_from_edit cotiene una lista : id_medico elejido y liststore(de editar_medicos.py)

        self.id_med=Values_from_edit[0]
        self.liststore_med=Values_from_edit[1]

        archivo="Vistas/Medicos/eliminar_medico.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)

        self.window=glade.get_object("dialog1")
        self.button_cancelar=glade.get_object("button_cancelar")
        self.button_aceptar=glade.get_object("button_aceptar")
        self.liststore_turnos=glade.get_object("liststore3")
        self.entry_nomyape=glade.get_object("entry_nomyape")
        self.entry_dni=glade.get_object("entry_dni")
        self.entry_nromat=glade.get_object("entry_nromat")
        self.entry_dom=glade.get_object("entry_dom")
        self.entry_tel=glade.get_object("entry_tel")

        columna_toggle=glade.get_object("treeviewcolumn6")
        self.cellrenderer_toggle = gtk.CellRendererToggle()
        columna_toggle.pack_start(self.cellrenderer_toggle, True)
        columna_toggle.add_attribute(self.cellrenderer_toggle, "active", 5)

        values=Consulta_Medico(self.id_med)
        self.mostrar_datos(values)

        lista=TodosTurnosPacMed("id_medico",self.id_med)
        for tur in lista:
            self.liststore_turnos.append([ tur.getHora(),
                                Acomodar_Fecha( tur.getFecha() ),
                                tur.getPaciente(),
                                tur.getMedico(),
                                tur.getCausa(),
                                tur.getEstado() ] )

        self.button_cancelar.connect("clicked",self.cerrar)
        self.window.connect("delete_event",self.delete_event)
        self.button_aceptar.connect("clicked",self.borrar_medico)

    def mostrar_datos(self,values):
        self.entry_nomyape.set_text(values.getNomyApe() )
        self.entry_dni.set_text(str( values.getDni()  ) )
        self.entry_nromat.set_text(str( values.getNroMat()  ) )
        self.entry_tel.set_text(values.getTelefono()  )
        self.entry_dom.set_text(values.getDomicilio()  )
