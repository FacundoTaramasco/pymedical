#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk

from Modelos.Model_Medico import Todos_Medicos
from Modelos.Model_Turno import ExistenciaFechaNULL

from Controladores.Utilidades import Fecha_Para_Calendar

from Controladores.Medicos.Eliminar.eliminar_medico import Eliminar_Medico
from Controladores.Medicos.Modificar.modificar_medico import Modificar_Medico

class Editar_Medicos():

    def eliminar_medico(self,widget):
        Values_from_edit=(self.id_medico,self.liststore_medicos)
        Eliminar_Medico(Values_from_edit)

    def modificar_medico(self,widget):
        Values_from_edit=(self.id_medico,self.liststore_medicos)
        Modificar_Medico(Values_from_edit)

    def target(self,iter):
        self.id_medico=self.liststore_medicos.get_value(iter,0)

    def rightClick(self,widget,event):
        click = event.button
        pos = (event.x, event.y)
        tiempo = event.time
        if click == 3:
            self.menu(widget, click, pos, tiempo)

    def delete_event(self,widget,event):
        self.buttonfechanull()
        self.updatecombomed()
        self.window.destroy()

    def cerrar(self,widget):
        self.buttonfechanull()
        self.updatecombomed()
        self.window.destroy()

    def __init__(self,Values_from_main):

        self.combobox_medicos_main=Values_from_main[0]
        self.liststore_medicos_main=Values_from_main[1]
        self.treeview_main=Values_from_main[2]
        self.button_turnos_null=Values_from_main[3]

        archivo="Vistas/Medicos/editar_medicos.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)

        self.window=glade.get_object("dialog1")
        self.button_cerrar=glade.get_object("button_cerrar")
        self.liststore_medicos=glade.get_object("liststore1")
        self.treeview=glade.get_object("treeview1")

        self.treeselection = self.treeview.get_selection()

        self.agregando_medicos()

        self.treeview.add_events(gtk.gdk.BUTTON2_MASK)
        self.treeview.connect("button-press-event", self.rightClick)

        self.button_cerrar.connect("clicked",self.cerrar)
        self.window.connect("delete_event",self.delete_event)

    def agregando_medicos(self):
        self.liststore_medicos.clear()
        lista=Todos_Medicos()
        for Med in lista:
            self.liststore_medicos.append([ Med.getId(),
                                            Med.getNomyApe(),
                                            Med.getDni(),
                                            Med.getNroMat(),
                                            Med.getTelefono(),
                                            Med.getDomicilio() ])

    def menu(self, widget, boton, pos, tiempo):
        (model, iter) = self.treeselection.get_selected()
        menu = gtk.Menu()
        delete=gtk.Image()
        modific=gtk.Image()
        delete.set_from_file("Recursos/remove2.png")
        modific.set_from_file("Recursos/modific.png")
        eliminar = gtk.ImageMenuItem()
        actualizar = gtk.ImageMenuItem()
        eliminar.set_image(delete)
        actualizar.set_image(modific)
        eliminar.set_label("Eliminar Medico")
        actualizar.set_label("Modificar Medico")
        menu.append(eliminar)
        menu.append(actualizar)
        eliminar.connect("activate", self.eliminar_medico)
        actualizar.connect("activate", self.modificar_medico)
        if iter != None:
            self.target(iter)
            menu.show_all()
            menu.popup(None, None,None, boton, tiempo)

    def updatecombomed(self):
        #Cargo la lista del combobox con los medicos existentes (de Core.py)
        #self.combobox_medicos_main.handler_block(self.obj_id)
        self.liststore_medicos_main.clear()
        lista=Todos_Medicos()
        for medic in lista:
            self.liststore_medicos_main.append( [medic.getNomyApe()] )
        self.combobox_medicos_main.set_active(0)
        #habilito el treeview llegado el caso de que este desactivado
        #(si no existieran medicos)
        if lista:self.treeview_main.set_sensitive(True)
        else:self.treeview_main.set_sensitive(False)

    def buttonfechanull(self):
        if ExistenciaFechaNULL():
            self.button_turnos_null.show()
        else:
            self.button_turnos_null.hide()
