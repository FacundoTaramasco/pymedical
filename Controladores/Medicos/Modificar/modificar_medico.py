#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk

from Controladores.Utilidades import es_int
from Controladores.Utilidades import Acomodar_Fecha
from Controladores.Utilidades import Fecha_Para_Calendar
from Controladores.Utilidades import FechaADiaSemana
from Controladores.Utilidades import caracteres_validos
from Controladores.Utilidades import caracteres_validos_telefono

from Modelos.Model_Turno import ModFechaNULL
from Modelos.Model_Turno import TodosTurnosPacMed
from Modelos.Model_Turno import TurnosDiaSemana

from Modelos.Model_Medico import Medico
from Modelos.Model_Medico import Consulta_Medico
from Modelos.Model_Medico import ConsultaEspecificaMED
from Modelos.Model_Medico import Todos_Medicos
from Modelos.Model_Medico import Modificacion_Medico

class Modificar_Medico():

    def update(self,widget):
        #tomando datos
        nomyape=self.entry_nomyape.get_text()
        dni=self.entry_dni.get_text()
        nromat=self.entry_nromat.get_text()
        telefono=self.entry_tel.get_text()
        domicilio=self.entry_dom.get_text()
        #si el medico trabajaba los lunes y deja de trabajar, todas las fechas
        #de esos turnos (si existen turnos) se setean a 'NULL'
        if self.existeturnolunes and not self.checkbutton_lunes.get_active():
            ModFechaNULL(self.id_med,self.dicdia["lunes"])
        if self.existeturnomartes and not self.checkbutton_martes.get_active():
            ModFechaNULL(self.id_med,self.dicdia["martes"])
        if self.existeturnomiercoles and not self.checkbutton_miercoles.get_active():
            ModFechaNULL(self.id_med,self.dicdia["miercoles"])
        if self.existeturnojueves and not self.checkbutton_jueves.get_active():
            ModFechaNULL(self.id_med,self.dicdia["jueves"])
        if self.existeturnoviernes and not self.checkbutton_viernes.get_active():
            ModFechaNULL(self.id_med,self.dicdia["viernes"])
        if self.existeturnosabado and not self.checkbutton_sabado.get_active():
            ModFechaNULL(self.id_med,self.dicdia["sabado"])
        if self.existeturnodomingo and not self.checkbutton_domingo.get_active():
            ModFechaNULL(self.id_med,self.dicdia["domingo"])
        #seteando datos medico
        Med=Medico()
        Med.setId(self.id_med)
        Med.setNomyApe(nomyape)
        Med.setDni(dni)
        Med.setNroMat(nromat)
        Med.setTelefono(telefono)
        Med.setDomicilio(domicilio)
        Med.setTrabajaLunes(self.checkbutton_lunes.get_active())
        Med.setTrabajaMartes(self.checkbutton_martes.get_active())
        Med.setTrabajaMiercoles(self.checkbutton_miercoles.get_active())
        Med.setTrabajaJueves(self.checkbutton_jueves.get_active())
        Med.setTrabajaViernes(self.checkbutton_viernes.get_active())
        Med.setTrabajaSabado(self.checkbutton_sabado.get_active())
        Med.setTrabajaDomingo(self.checkbutton_domingo.get_active())
        #modificando medico en la bd
        Modificacion_Medico(Med)
        #Actualizo el liststore de editar-medicos.py
        self.liststore_med.clear()
        lista=Todos_Medicos()
        for Med in lista:
            self.liststore_med.append([Med.getId(),
                                        Med.getNomyApe(),
                                        Med.getDni(),
                                        Med.getNroMat(),
                                        Med.getTelefono(),
                                        Med.getDomicilio() ])
        self.window.destroy()

    def functionverturnos(self,widget):
        if widget.get_active():
            self.notebook.show()
        else : self.notebook.hide()

    def validar_nomyape(self,widget):
        self.nomyape_ok=False
        nomyape=self.entry_nomyape.get_text()
        if caracteres_validos(nomyape) and nomyape != "":
            if (ConsultaEspecificaMED("id","nomyape",nomyape) == None) or\
            (ConsultaEspecificaMED("id","nomyape",nomyape) == self.id_med):
                self.entry_nomyape.set_icon_from_stock(1,gtk.STOCK_OK)
                self.entry_nomyape.set_property("secondary-icon-tooltip-text","")
                self.nomyape_ok=True
            else:
                self.entry_nomyape.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
                self.entry_nomyape.set_property("secondary-icon-tooltip-text",
                                                "Medico existente")
        else:
            self.entry_nomyape.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_nomyape.set_property("secondary-icon-tooltip-text",
                                            "Incorrecto")

    def validar_dni(self,widget):
        self.dni_ok=False
        dni=self.entry_dni.get_text()
        if es_int(dni) and dni != "":
            if (ConsultaEspecificaMED("id","dni",dni) == None) or\
            (ConsultaEspecificaMED("id","dni",dni) == self.id_med):
                self.entry_dni.set_icon_from_stock(1,gtk.STOCK_OK)
                self.entry_dni.set_property("secondary-icon-tooltip-text","")
                self.dni_ok=True
            else:
                self.entry_dni.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
                self.entry_dni.set_property("secondary-icon-tooltip-text",
                                            "DNI existente")
        else:
            self.entry_dni.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_dni.set_property("secondary-icon-tooltip-text","Incorrecto")

    def validar_nromat(self,widget):
        self.nromat_ok=False
        nromat=self.entry_nromat.get_text()
        if es_int(nromat) and nromat != "":
            if (ConsultaEspecificaMED("id","nromat",nromat) == None) or\
            (ConsultaEspecificaMED("id","nromat",nromat) == self.id_med):
                self.entry_nromat.set_icon_from_stock(1,gtk.STOCK_OK)
                self.entry_nromat.set_property("secondary-icon-tooltip-text","")
                self.nromat_ok=True
            else:
                self.entry_nromat.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
                self.entry_nromat.set_property("secondary-icon-tooltip-text",
                                                "Matricula existente")
        else:
            self.entry_nromat.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_nromat.set_property("secondary-icon-tooltip-text",
                                            "Incorrecto")

    def validar_telefono(self,widget):
        self.tel_ok=False
        telefono=self.entry_tel.get_text()
        if caracteres_validos_telefono(telefono) and telefono != "":
            self.entry_tel.set_icon_from_stock(1,gtk.STOCK_OK)
            self.entry_tel.set_property("secondary-icon-tooltip-text","")
            self.tel_ok=True
        else:
            self.entry_tel.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_tel.set_property("secondary-icon-tooltip-text",
                                        "Incorrecto")

    def validar_domicilio(self,widget):
        self.dom_ok=False
        domicilio=self.entry_dom.get_text()
        if caracteres_validos(domicilio) and domicilio != "":
            self.entry_dom.set_icon_from_stock(1,gtk.STOCK_OK)
            self.entry_dom.set_property("secondary-icon-tooltip-text","")
            self.dom_ok=True
        else:
            self.entry_dom.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_dom.set_property("secondary-icon-tooltip-text",
                                        "Incorrecto")

    def desbloquear_boton_aceptar(self,widget):
        if self.nomyape_ok and self.dni_ok and  self.nromat_ok and\
        self.dom_ok and self.tel_ok:
            self.button_aceptar.set_sensitive(True)
        else:
            self.button_aceptar.set_sensitive(False)

    def delete_event(self,widget,event):
        self.window.destroy()

    def cerrar(self,widget):
        self.window.destroy()

    def __init__(self,Values_from_edit):
        #Values_from_edit cotiene una lista : id_medico elejido
        #y liststore(de editar_medicos.py)
        self.id_med=Values_from_edit[0]
        self.liststore_med=Values_from_edit[1]

        #utilizado en una query en bd turnos
        self.dicdia={"domingo": "0",
                "lunes" : "1",
                "martes" : "2",
                "miercoles" : "3",
                "jueves" : "4",
                "viernes" : "5",
                "sabado" : "6"}

        archivo="Vistas/Medicos/modificar_medico.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)

        self.window=glade.get_object("dialog1")
        self.button_aceptar=glade.get_object("button_aceptar")
        self.button_cancelar=glade.get_object("button_cancelar")

        self.notebook=glade.get_object("notebook1")
        self.label_msj=glade.get_object("label6")
        self.buttonverturnos = glade.get_object("togglebutton1")

        self.entry_nomyape=glade.get_object("entry_nomyape")
        self.entry_dni=glade.get_object("entry_dni")
        self.entry_nromat=glade.get_object("entry_nromat")
        self.entry_tel=glade.get_object("entry_tel")
        self.entry_dom=glade.get_object("entry_dom")

        self.checkbutton_lunes=glade.get_object("checkbutton1")
        self.checkbutton_martes=glade.get_object("checkbutton2")
        self.checkbutton_miercoles=glade.get_object("checkbutton3")
        self.checkbutton_jueves=glade.get_object("checkbutton4")
        self.checkbutton_viernes=glade.get_object("checkbutton5")
        self.checkbutton_sabado=glade.get_object("checkbutton6")
        self.checkbutton_domingo=glade.get_object("checkbutton7")

        Medic_Actual=Consulta_Medico(self.id_med)
        self.mostrar_datos(Medic_Actual)

        (self.existeturnolunes,
        self.existeturnomartes,
        self.existeturnomiercoles,
        self.existeturnojueves,
        self.existeturnoviernes,
        self.existeturnosabado,
        self.existeturnodomingo)=(False,False,False,False,False,False,False)

        msj="¡Atención! Existen turnos los dias :\n"

        #si esta activado es xq el medico trabaja los lunes
        if self.checkbutton_lunes.get_active():
            #tomo los turnos del medico 'self.id_med' y del dia de semana lunes
            turs=TurnosDiaSemana(self.id_med,self.dicdia["lunes"])
            #si existen turnos los dias lunes lo informo
            if turs :
                #cargo el liststore de los lunes
                self.cargarturnosdia(turs,glade.get_object("liststore1"))
                self.existeturnolunes=True
                msj+="*Lunes\n"
        if self.checkbutton_martes.get_active():
            turs=TurnosDiaSemana(self.id_med,self.dicdia["martes"])
            if turs :
                self.cargarturnosdia(turs,glade.get_object("liststore2"))
                self.existeturnomartes=True
                msj+="*Martes\n"
        if self.checkbutton_miercoles.get_active():
            turs=TurnosDiaSemana(self.id_med,self.dicdia["miercoles"])
            if turs :
                self.cargarturnosdia(turs,glade.get_object("liststore3"))
                self.existeturnomiercoles=True
                msj+="*Miercoles\n"
        if self.checkbutton_jueves.get_active():
            turs=TurnosDiaSemana(self.id_med,self.dicdia["jueves"])
            if turs :
                self.cargarturnosdia(turs,glade.get_object("liststore4"))
                self.existeturnojueves=True
                msj+="*Jueves\n"
        if self.checkbutton_viernes.get_active():
            turs=TurnosDiaSemana(self.id_med,self.dicdia["viernes"])
            if turs :
                self.cargarturnosdia(turs,glade.get_object("liststore5"))
                self.existeturnoviernes=True
                msj+="*Viernes\n"
        if self.checkbutton_sabado.get_active():
            turs=TurnosDiaSemana(self.id_med,self.dicdia["sabado"])
            if turs :
                self.cargarturnosdia(turs,glade.get_object("liststore6"))
                self.existeturnosabado=True
                msj+="*Sabado\n"
        if self.checkbutton_domingo.get_active():
            turs=TurnosDiaSemana(self.id_med,self.dicdia["domingo"])
            if turs :
                self.cargarturnosdia(turs,glade.get_object("liststore7"))
                self.existeturnodomingo=True
                msj+="*Domingo\n"

        msj+="Al quitar un dia donde se encuntran turnos\n"\
        +"estos mismos son seteados a fecha NULL"

        #si en alguno de los dias el medico trabaja y existen turnos
        if self.existeturnolunes or self.existeturnomartes or\
        self.existeturnomiercoles or self.existeturnojueves or\
        self.existeturnoviernes or self.existeturnosabado or\
        self.existeturnodomingo:
            self.label_msj.set_text(msj)
            self.buttonverturnos.show()

        (self.nomyape_ok,
        self.dni_ok,
        self.nromat_ok,
        self.dom_ok,
        self.tel_ok)=(True,True,True,True,True)

        self.entry_nomyape.connect("changed",self.validar_nomyape)
        self.entry_dni.connect("changed",self.validar_dni)
        self.entry_nromat.connect("changed",self.validar_nromat)
        self.entry_tel.connect("changed",self.validar_telefono)
        self.entry_dom.connect("changed",self.validar_domicilio)

        self.entry_nomyape.connect("changed",self.desbloquear_boton_aceptar)
        self.entry_dni.connect("changed",self.desbloquear_boton_aceptar)
        self.entry_nromat.connect("changed",self.desbloquear_boton_aceptar)
        self.entry_dom.connect("changed",self.desbloquear_boton_aceptar)
        self.entry_tel.connect("changed",self.desbloquear_boton_aceptar)

        self.buttonverturnos.connect("toggled",self.functionverturnos)
        self.button_aceptar.connect("clicked",self.update)
        self.button_cancelar.connect("clicked",self.cerrar)
        self.window.connect("delete_event",self.delete_event)

    def mostrar_datos(self,Medic_Actual):
        self.entry_nomyape.set_text(Medic_Actual.getNomyApe())
        self.entry_dni.set_text(str(Medic_Actual.getDni()))
        self.entry_nromat.set_text(str(Medic_Actual.getNroMat()))
        self.entry_tel.set_text(Medic_Actual.getTelefono())
        self.entry_dom.set_text(Medic_Actual.getDomicilio())
        self.checkbutton_lunes.set_active(Medic_Actual.getTrabajaLunes())
        self.checkbutton_martes.set_active(Medic_Actual.getTrabajaMartes())
        self.checkbutton_miercoles.set_active(Medic_Actual.getTrabajaMiercoles())
        self.checkbutton_jueves.set_active(Medic_Actual.getTrabajaJueves())
        self.checkbutton_viernes.set_active(Medic_Actual.getTrabajaViernes())
        self.checkbutton_sabado.set_active(Medic_Actual.getTrabajaSabado())
        self.checkbutton_domingo.set_active(Medic_Actual.getTrabajaDomingo())

    def cargarturnosdia(self,turs,liststore):
        """Cargo en un liststore los turnos dependiendo del dia de semana"""
        for Turn in turs:
            liststore.append([Turn.getHora(),
                            Acomodar_Fecha(Turn.getFecha()),
                            Turn.getPaciente(),
                            Turn.getMedico(),
                            Turn.getCausa(),
                            Turn.getEstado()])
