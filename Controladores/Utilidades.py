#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import datetime

from Modelos.Model_Turno import Turno
from Modelos.Model_Turno import Turnos_Especificos

from Modelos.Model_Medico import ConsultaEspecificaMED

def Lista_Horarios():
    """Funcion que retorna una lista con todos
    los horarios usados en la aplicacion.
    Es usado para cargar comboboxs y liststores"""
    lista=[]
    manejador=open("Controladores/Sistema_horario.csv","r")
    manejador_csv=csv.reader(manejador)
    for line in manejador_csv:
        lista.append(line[0])
    manejador.close()
    return lista

def Cantidad_dia_mes(anio,mes):
    """Funcion que retorna la cantidad de dias de un mes"""
    if(anio % 4 == 0 and anio % 100 != 0 or anio % 400 == 0): #bisiesto
        lista_meses=[31,29,31,30,31,30,31,31,30,31,30,31]
    else:
        lista_meses=[31,28,31,30,31,30,31,31,30,31,30,31]
    return str(lista_meses[mes] )

def Acomodar_Fecha(fecha):
    """si recibe formato de fecha : AAAA-MM-DD , retorna DD-MM-AAAA
    si recibe formato de fecha : DD-MM-AAAA , retorna AAAA-MM-DD"""
    val1,val2,val3=fecha.split("-")
    return val3+"-"+val2+"-"+val1



def Acomodar_dia_mes(dia,mes):
    """calendar.get_date() devuelve por ej: 1,2,3,etc.Le agrego el 0 : 01,02,03,etc"""
    mes+=1 #el get.date trabaja los meses asi : (0...11)
    mes=str(mes)
    dia=str(dia)
    if len(mes) == 1 : mes="0"+mes
    if len(dia) == 1 : dia="0"+dia
    return dia,mes

def Fecha_Para_Calendar(fecha):
    """Recibe una fecha dd-mm-aaaaa en string
    y retorna int(dd) int(mes) int(aaaa)
    usado para posicionar en gtk.calendar"""
    dia,mes,anio = fecha.split("-")
    return int(dia),int(mes)-1,int(anio)


def FechaADiaSemana(dia,mes,anio):
    #para calcular dia de la semana
    fecha = datetime.date(anio, mes+1, dia)
    #dia de semana.Ej. 'lunes','martes',etc
    dia_semana=fecha.strftime('%A').replace("á","a").replace("é","e")
    return dia_semana

def Limpiar_treeview_turnos(liststore):
    """Recibe un liststore y lo setea"""
    for fila in liststore:
        fila[0]=1
        fila[2]=""
        fila[3]=""
        fila[4]=False


def Cargar_treeview_turnos(medico,fecha,liststore):
    """Recibe ciertos parametros para cargar un liststore con turnos
    especificos dependiendo su fecha-medico"""
    Tur=Turno()
    Tur.setIdMed( ConsultaEspecificaMED("id","nomyape",medico) )
    Tur.setFecha(fecha)
    lista=Turnos_Especificos(Tur)
    for turn in lista:
        for fila in liststore:
            if fila[1] == turn.getHora():
                fila[0]=turn.getId()
                fila[2]=turn.getPaciente()
                fila[3]=turn.getCausa()
                if turn.getEstado():
                    fila[4]=turn.getEstado()


def es_int(valor):
    try:
        int(valor)
        return True
    except:
        return False

def caracteres_validos(palabra):
    try:
        for letra in palabra:
            #si no es mayuscula y no es miniscula y no es espacio y no es " y no es *
            if not ( 65 <=ord(letra) <=90 )and not (97<=ord(letra)<= 122) and not ord(letra) == 32 and not ord(letra) == 34 and not ord(letra)==42:
                #si no es numero,si no es   , - . /  y no es ( )
                if not ( 48 <=ord(letra) <=57 ) and not ( 44 <=ord(letra) <=47 ) and not ( 40 <=ord(letra) <=41 ) :
                    return False
        return True
    except:
        return False


def caracteres_validos_telefono(palabra):
    try:
        for letra in palabra:
            if not ord(letra) == 32: #si no es espacio
                if not ( 48 <=ord(letra) <=57 ) and not ( ord(letra) ==45 ) and not ( 40 <=ord(letra) <=41 ): #si no es numero,si no es - y no es ( )
                    return False
        return True
    except:
        return False

