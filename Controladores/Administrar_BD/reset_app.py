#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk

class Reiniciar_Aplicacion():
    """Esta clase es para pedirle que cierre
        y abra el programa luego de especificar una nueva BD o crearla
        para evitar bugs
    """

    def reiniciar_app(self,widget):
        gtk.main_quit()

    def delete_event(self,widget,event):
        gtk.main_quit()

    def __init__(self):
        archivo="Vistas/Administrar_BD/mensaje_reiniciar.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)
        window=glade.get_object("dialog1")
        button=glade.get_object("button_aceptar")
        button.connect("clicked",self.reiniciar_app)
        window.connect("delete_event",self.delete_event)