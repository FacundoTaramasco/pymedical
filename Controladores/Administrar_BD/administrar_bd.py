#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk

from new_bbdd import New_BBDD

from Modelos.Model_BD import Leer_Ruta_Turnos

from Controladores.Administrar_BD.guardar_bd import Guardar_BBDD
from Controladores.Administrar_BD.especificar_bd import Especificar_BBDD

class Administrar_BBDD():

    def backup(self,widget):
        Guardar_BBDD()

    def especificar(self,widget):
        Especificar_BBDD(self.window)

    def crear_base_de_datos(self,widget):
        New_BBDD(self.window)

    def salir(self,widget):
        self.window.destroy()

    def delete_event(self,widget,event):
        self.window.destroy()

    def __init__(self):

        archivo="Vistas/Administrar_BD/administrar_BD.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)

        self.window=glade.get_object("window1")
        self.button_cerrar=glade.get_object("button_cerrar")
        self.entry_BD=glade.get_object("entry_BD")
        self.button_nuevo=glade.get_object("button_nuevo")
        self.button_guardar=glade.get_object("button_guardar")
        self.button_especificar=glade.get_object("button_especificar")

        self.entry_BD.set_text(Leer_Ruta_Turnos()[:-7] )

        self.window.connect("delete_event",self.delete_event)
        self.button_cerrar.connect("clicked",self.salir)
        self.button_nuevo.connect("clicked",self.crear_base_de_datos)
        self.button_guardar.connect("clicked",self.backup)
        self.button_especificar.connect("clicked",self.especificar)
