#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk

from Modelos.Model_BD import Guardar_Ruta_Turnos
from Modelos.Model_BD import Guardar_Ruta_Pacientes
from Modelos.Model_BD import Guardar_Ruta_Medicos

from Modelos.Model_BD import Verificando_Nueva_Ruta_BBDD

from reset_app import Reiniciar_Aplicacion

class Especificar_BBDD():

    def salir(self,widget):
        self.dialog.destroy()

    def delete_event(self,widget,event):
        self.dialog.destroy()

    def __init__(self,win):

        dialog = gtk.FileChooserDialog("Especificar Base de Datos",None,gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN, gtk.RESPONSE_OK))
        dialog.set_icon_from_file("Recursos/plus_shield.png")
        dialog.set_default_response(gtk.RESPONSE_OK)

        response = dialog.run()
        if response == gtk.RESPONSE_OK:
            carpeta_BBDD=dialog.get_filename()
            if Verificando_Nueva_Ruta_BBDD(carpeta_BBDD):
                Guardar_Ruta_Turnos(carpeta_BBDD)
                Guardar_Ruta_Pacientes(carpeta_BBDD)
                Guardar_Ruta_Medicos(carpeta_BBDD)
                win.destroy()
                Reiniciar_Aplicacion()
        dialog.destroy()
