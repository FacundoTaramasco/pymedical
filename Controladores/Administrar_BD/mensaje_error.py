#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk

from new_bbdd import New_BBDD

from Controladores.Administrar_BD.especificar_bd import Especificar_BBDD



class Mensaje_Error():
    """Si se produce un error al conectar a la base de datos soy invocado"""

    def especificar(self,widget):
        Especificar_BBDD(self.window)

    def crear_bbdd(self,widget):
        New_BBDD(self.window)

    def salir(self,widget):
        gtk.main_quit()

    def delete_event(self,widget,event):
        gtk.main_quit()

    def __init__(self):

        archivo="Vistas/Administrar_BD/error.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)

        self.window=glade.get_object("dialog1")
        self.window.set_icon_from_file("Recursos/plus_shield.png")
        self.button_new_bbdd=glade.get_object("button_new")
        self.button_cancelar=glade.get_object("button_cancelar")
        self.button_especificar=glade.get_object("button_especificar")

        self.window.connect("delete_event",self.delete_event)
        self.button_cancelar.connect("clicked",self.salir)
        self.button_especificar.connect("clicked",self.especificar)
        self.button_new_bbdd.connect("clicked",self.crear_bbdd)
