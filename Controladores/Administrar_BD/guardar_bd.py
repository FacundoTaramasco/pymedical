#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk
import os
import time
import shutil

from Modelos.Model_BD import Leer_Ruta_Medicos
from Modelos.Model_BD import Leer_Ruta_Pacientes
from Modelos.Model_BD import Leer_Ruta_Turnos


class Guardar_BBDD():

    def dividir_hora(self):
        hora_actual=str(time.ctime())
        hora_actual=hora_actual.replace(" ","-")
        hora_actual=hora_actual.replace(":","-")
        return hora_actual

    def realizar_backup(self,widget,event):
        ruta=self.dialog.get_current_folder()
        if ruta != None:
            carpeta=ruta+"/BACKUP-"+self.dividir_hora()
            os.mkdir(carpeta)
            shutil.copyfile(Leer_Ruta_Medicos(),carpeta+"/medicos")
            shutil.copyfile(Leer_Ruta_Turnos(),carpeta+"/turnos")
            shutil.copyfile(Leer_Ruta_Pacientes(),carpeta+"/pacientes")
            self.dialog.destroy()

    def salir(self,widget):
        self.dialog.destroy()

    def delete_event(self,widget,event):
        self.dialog.destroy()

    def __init__(self):

        archivo="Vistas/Administrar_BD/guardar_BD.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)

        self.dialog=glade.get_object("filechooserdialog1")
        self.button_guardar=glade.get_object("button_guardar")
        self.button_cancelar=glade.get_object("button_cancelar")

        self.dialog.connect("delete_event",self.delete_event)
        self.button_cancelar.connect("clicked",self.salir)
        self.dialog.connect("response",self.realizar_backup)

