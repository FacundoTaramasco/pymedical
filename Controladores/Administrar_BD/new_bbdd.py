#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk

from reset_app import Reiniciar_Aplicacion

from Modelos.Model_BD import Crear_Nueva_BBDD
from Modelos.Model_BD import Guardar_Ruta_Turnos
from Modelos.Model_BD import Guardar_Ruta_Pacientes
from Modelos.Model_BD import Guardar_Ruta_Medicos

class New_BBDD():
    def __init__(self,win_admin):
        filechooserdialog = gtk.FileChooserDialog("Crear Nueva Base de Datos",
                                                None,
                                                gtk.FILE_CHOOSER_ACTION_CREATE_FOLDER,
                                                (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                                gtk.STOCK_OK, gtk.RESPONSE_OK))

        response = filechooserdialog.run()

        if response == gtk.RESPONSE_OK:
            path=filechooserdialog.get_filename()
            win_admin.destroy()
            Crear_Nueva_BBDD(path)
            Guardar_Ruta_Turnos(path)
            Guardar_Ruta_Pacientes(path)
            Guardar_Ruta_Medicos(path)
            Reiniciar_Aplicacion()
        filechooserdialog.destroy()