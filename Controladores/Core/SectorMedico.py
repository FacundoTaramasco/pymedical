#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk
import pygtk

pygtk.require('2.0')

from Modelos.Model_Medico import Todos_Medicos

from Controladores.Medicos.Agregar.agregar_medico import Nuevo_Medico
from Controladores.Medicos.Editar_medicos.editar_medicos import Editar_Medicos

class SectorMedico():

    def function_agregar_medico(self,widget):
        """Ventana para agregar medico"""
        Values_from_main=(self.combobox_medicos,
        self.liststore_medicos,
        self.treeview)
        Nuevo_Medico(Values_from_main)

    def function_editar_medicos(self,widget):
        """Ventana para editar medicos(eliminar,modificar)"""
        Values_from_main=(self.combobox_medicos,
        self.liststore_medicos,
        self.treeview,
        self.button_turnos_null)
        Editar_Medicos(Values_from_main)

    def __init__(self):

        self.liststore_medicos=self.glade.get_object("liststore_medicos")
        self.combobox_medicos=self.glade.get_object("combobox_medicos")

        self.button_agregar_medico=self.glade.get_object("agregar_medico")
        self.button_editar_medicos=self.glade.get_object("editar_medicos")


        ################################## Conectando señales ################

        self.combobox_medicos.connect("changed",self.dia_elejido)

        self.button_agregar_medico.connect("clicked",self.function_agregar_medico)
        self.button_editar_medicos.connect("clicked",self.function_editar_medicos)

    def agregar_medicos_combobox(self):
        """Cargo la lista del combobox con los medicos existentes"""
        self.liststore_medicos.clear()
        lista=Todos_Medicos()
        if lista: #si existen medicos
            for Medico in lista:
                self.liststore_medicos.append( [Medico.getNomyApe()] )
            self.combobox_medicos.set_active(0)
            return True
        else:
            self.treeview.set_sensitive(False)
            return False
