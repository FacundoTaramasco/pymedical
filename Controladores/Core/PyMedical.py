 #!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk
import pygtk

pygtk.require('2.0')

if gtk.pygtk_version < (2,10,0):
    print "PyGtk 2.10.0 or later required"
    raise SystemExit(1)

#from Modelos.Model_Turno import wtftestquery
#wtftestquery()

from Controladores.Core.SectorTurno import SectorTurno
from Controladores.Core.SectorMedico import SectorMedico
from Controladores.Core.SectorPaciente import SectorPaciente

from Modelos.Model_Turno import Turnos_de_Mes

from Modelos.Model_Medico import Consulta_Medico
from Modelos.Model_Medico import ConsultaEspecificaMED

from Modelos.Model_BD import Verificando_Conexion_BBDD

#from Modelos.Model_BD import Crear_Nueva_BBDD
#Crear_Nueva_BBDD()
#from Modelos.Model_BD import Leer_Ruta_BBDD
#print Leer_Ruta_BBDD()

from Controladores.Utilidades import Cantidad_dia_mes
from Controladores.Utilidades import Acomodar_Fecha
from Controladores.Utilidades import Acomodar_dia_mes
from Controladores.Utilidades import FechaADiaSemana
from Controladores.Utilidades import Cargar_treeview_turnos
from Controladores.Utilidades import Limpiar_treeview_turnos

"""
from Controladores.Administrar_BD.guardar_bd import Guardar_BBDD
from Controladores.Administrar_BD.administrar_bd import Administrar_BBDD
from Controladores.Administrar_BD.mensaje_error import Mensaje_Error
"""

class PyMedical(SectorTurno,SectorPaciente,SectorMedico):
    """Clase principal de PyMedical"""

    ####################### Retrollamadas ####################################

    def realizar_backup(self,widget):
        """Ventana para realizar backup de bbdd"""
        Guardar_BBDD()

    def function_administrar_BBDD(self,widget):
        """Ventana para admin. bbdd(nueva,especificar otra,realizar backup)"""
        Administrar_BBDD()

    def dia_elejido(self,widget):
        """Conectado con gtk calendar(SectorTurno) y gtk combobox(SectorMedico)
        Al seleccionar un dia en el gtk calendar o cambiar medico en el gtk
        combobox medico busco turnos en la bbdd con dicho dia y medico elejido,
        para mostrarlos en el liststore
        ++(los dias del mes actual que contengan turnos se marcan en el calendar)
        ++(llegado el caso que el medico no trabaje en el dia de la semana
        seleccionado se informa)"""
        #tomo el medico elejido
        medico=self.combobox_medicos.get_active_text()
        #posiciono gtk notebook a la primera pagina
        self.notebook.set_current_page(0)
        #quito todas las marcas del gtk calendar
        self.calendar.clear_marks()
        if medico:
            #fecha seleccionada en el gtk calendar
            anio,mes,dia = self.calendar.get_date()
            dia_semana = FechaADiaSemana(dia,mes,anio)
            #del medico elejido tomo su id
            id_med=ConsultaEspecificaMED("id","nomyape",medico)
            #realizo una consulta del medico
            Med=Consulta_Medico(id_med)
            #verifico si el medico trabaja el dia elejido de la semana
            #si no trabaja lo informo
            if dia_semana == "lunes" and not Med.getTrabajaLunes():
                self.notebook.set_current_page(1)
                self.label_mensaje.set_text(medico+" no trabaja los lunes.")
            if dia_semana == "martes" and not Med.getTrabajaMartes():
                self.notebook.set_current_page(1)
                self.label_mensaje.set_text(medico+" no trabaja los mastes.")
            if dia_semana == "miercoles" and not Med.getTrabajaMiercoles():
                self.notebook.set_current_page(1)
                self.label_mensaje.set_text(medico+" no trabaja los miercoles.")
            if dia_semana == "jueves" and not Med.getTrabajaJueves():
                self.notebook.set_current_page(1)
                self.label_mensaje.set_text(medico+" no trabaja los jueves.")
            if dia_semana == "viernes" and not Med.getTrabajaViernes():
                self.notebook.set_current_page(1)
                self.label_mensaje.set_text(medico+" no trabaja los viernes.")
            if dia_semana == "sabado" and not Med.getTrabajaSabado():
                self.notebook.set_current_page(1)
                self.label_mensaje.set_text(medico+" no trabaja los sabados.")
            if dia_semana == "domingo" and not Med.getTrabajaDomingo():
                self.notebook.set_current_page(1)
                self.label_mensaje.set_text(medico+" no trabaja los domingos.")
                #txt=self.label_mensaje.get_text()
                #self.label_mensaje.set_text(txt+"\nBoludo!!!")
            #convierto a str
            anio=str(anio)
            # 1 --> 01 ... 2 --> 02 etc.
            dia,mes=Acomodar_dia_mes(dia,mes)
            #setear fecha elejida en el label.formato dd/mm/aaaa
            self.label_fecha.set_text(dia+"-"+mes+"-"+anio)
            #mostrar con marcas en el gtk calendar los turnos del mes actual
            self.marcas_en_calendar(anio,mes,id_med)
            #cambiar formato fecha dd/mm/aaaa --> aaaa/mm/dd.utilizada en la bbdd
            fecha=Acomodar_Fecha(self.label_fecha.get_text())
            #setear treeview de turnos
            Limpiar_treeview_turnos(self.liststore_turnos)
            #cargarlo con el medico y la fecha elejida
            Cargar_treeview_turnos(medico,fecha,self.liststore_turnos)

    def salir(self,widget):
        gtk.main_quit()

    def delete_event(self,widget,event):
        gtk.main_quit()


    ##########################################################################

    def __init__(self):

        archivo="Vistas/Core/main.glade"
        self.glade=gtk.Builder()
        self.glade.add_from_file(archivo)

        ################### Tomando objetos ##################################

        self.window=self.glade.get_object("window1")

        self.button_salir=self.glade.get_object("imagemenuitem1")

        self.button_backup_general=self.glade.get_object("backup_general")
        self.button_adminBD=self.glade.get_object("administrar_bd")

        SectorTurno.__init__(self)
        SectorMedico.__init__(self)
        SectorPaciente.__init__(self)

        ######################################################################

         #chequeo conexion con la bbdd
        if Verificando_Conexion_BBDD():
            if self.agregar_medicos_combobox(): #SectorMedico
                self.dia_elejido(None)
        else:
            Mensaje_Error()

        #self.treeview.add_events(gtk.gdk.BUTTON2_MASK)

        ################## Conectando señales ################################

        self.button_backup_general.connect("clicked",self.realizar_backup)
        self.button_adminBD.connect("activate",self.function_administrar_BBDD)

        self.button_salir.connect("activate",self.salir)
        self.window.connect("delete_event",self.delete_event)

        ######################################################################

    def marcas_en_calendar(self,anio,mes,id_med):
        """Marca en el gtk calendar los dias en que hay turnos"""
        findemes=Cantidad_dia_mes(int(anio),int(mes)-1 )
        fecha_inicial=anio+"-"+mes+"-01"
        fecha_final=anio+"-"+mes+"-"+findemes
        dias_con_turno=Turnos_de_Mes(fecha_inicial,fecha_final,id_med)
        for d in dias_con_turno:
            self.calendar.mark_day(int(d))


    def run(self):
        gtk.main()

