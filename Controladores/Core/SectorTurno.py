 #!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk
import pygtk

pygtk.require('2.0')

from Modelos.Model_Turno import Turno

from Modelos.Model_Turno import ExistenciaFechaNULL
from Modelos.Model_Turno import TodosTurnosPacMed
from Modelos.Model_Turno import Actualizar_Estado

from Modelos.Model_Paciente import ConsultaEspecificaPAC

from Controladores.Turnos.Agregar.agregar_turno import Agregar_Turno
from Controladores.Turnos.Eliminar.eliminar_turno import Eliminar_Turno
from Controladores.Turnos.Modificar.modificar_turno import Modificar_Turno
from Controladores.Turnos.Fechas_null.fechasnull import TurnosFechasNULL
from Controladores.Turnos.Limpieza.limpieza_turnos import Limpieza_Turnos
from Controladores.Turnos.Historial.historial_turnos import Historial_Turnos


from Controladores.Utilidades import Acomodar_Fecha
from Controladores.Utilidades import Lista_Horarios

class SectorTurno():

    def function_agregar_turno(self,widget,iter):
        """Ventana para agregar turno"""
        Values_from_main=(self.combobox_medicos.get_active_text(),
        self.label_fecha.get_text(),
        self.liststore_turnos.get_value(iter,1),#hora
        self.calendar )
        Agregar_Turno(Values_from_main)

    def function_eliminar_turno(self,widget,iter):
        """Ventana para eliminar turno"""
        Values_from_main=( self.liststore_turnos.get_value(iter,0), #id
        self.calendar)
        Eliminar_Turno(Values_from_main)

    def function_modificar_turno(self,widget,iter):
        """Ventana para modificar turno"""
        Values_from_main=( self.liststore_turnos.get_value(iter,0), #id
        self.calendar)
        Modificar_Turno(Values_from_main)

    def function_turnos_null(self,widget):
        Values_from_main=(self.calendar,self.button_turnos_null)
        TurnosFechasNULL(Values_from_main)

    def function_historial_turnos(self,widget):
        """Ventana que muestra el historial de turnos"""
        Historial_Turnos()

    def function_limpieza_turnos(self,widget):
        """Ventana para realizar limpieza de turnos segun fecha y horario"""
        Values_from_main=(self.combobox_medicos.get_active_text(),
        self.label_fecha.get_text(),
        self.liststore_turnos,
        self.calendar)
        Limpieza_Turnos(Values_from_main)

    def calcular_turno_siguiente(self,widget):
        """Un tooltip del treeview que muestra el Proximo turno de un paciente"""
        (model, iter) = self.treeselection.get_selected()
        paciente=self.liststore_turnos.get_value(iter,2)
        tooltip=gtk.Tooltips()
        tooltip.set_tip(self.treeview, "")
        if paciente != "": #si existe un turno
            fecha=Acomodar_Fecha(self.label_fecha.get_text())
            hora=self.liststore_turnos.get_value(iter,1)
            prox_turno,existe_proximo=False,False
            id_pac=ConsultaEspecificaPAC("id","nomyape",paciente)
            lista=TodosTurnosPacMed("id_paciente",id_pac)
            for turn in lista:
                if prox_turno :
                    #si entro aca es porque existe un turno siguiente al elejido
                    hora_turno=turn.getHora()
                    fecha_turno=turn.getFecha()
                    medico_turno=turn.getMedico()
                    existe_proximo=True
                    break
                #esto es el turno elejido
                if turn.getFecha() == fecha and turn.getHora() == hora:
                    prox_turno=True
            if existe_proximo:
                tooltip.set_tip(self.treeview, "Proximo turno :\n   Fecha : "+Acomodar_Fecha(fecha_turno)
                    +"\n   Hora : "+hora_turno
                    +"\n   Medico : "+medico_turno)
            else:
                tooltip.set_tip(self.treeview, "Ultimo turno de :   "+paciente)

    def rightClick(self,widget,event):
        """Funcion llamada al realizar click derecho en el treeview"""
        click = event.button
        pos = (event.x, event.y)
        tiempo = event.time
        if click == 3: #click derecho
            self.menu(widget, click, pos, tiempo)

    def cell_toggled (self, widget, path, model):
        """Modifica el estado del turno"""
        if model[path][2] != "": #solo se modifica si hay un turno cargado
            model[path][4]= not model[path][4]
            Tur=Turno()
            Tur.setId(model[path][0])
            Tur.setEstado(model[path][4])
            Actualizar_Estado(Tur)

    def __init__(self):

        self.treeview=self.glade.get_object("treeview1")
        self.liststore_turnos=self.glade.get_object("liststore_turnos")

        self.notebook=self.glade.get_object("notebook1")
        self.label_mensaje=self.glade.get_object("label11")
        self.treeselection = self.treeview.get_selection()

        self.calendar=self.glade.get_object("calendar1")
        self.label_fecha=self.glade.get_object("label_fecha")

        self.cellrenderer_toggle=self.glade.get_object("cellrenderertoggle1")

        self.button_turnos_null=self.glade.get_object("turnosnull")
        self.button_historial_turnos=self.glade.get_object("historial_turnos")
        self.button_limpieza_turnos=self.glade.get_object("limpieza_turnos")

        ######################################################################

        #estructura del liststore
        for x,hora in enumerate(Lista_Horarios()):
             #x%2 --> es para darle el color alternado
            self.liststore_turnos.append([1,hora,"","",False,x%2] )

        self.cell_color()

        if ExistenciaFechaNULL():
            self.button_turnos_null.show()
        self.button_turnos_null.connect("clicked",self.function_turnos_null)

        ################################## Conectando señales ################

        self.calendar.connect("day-selected",self.dia_elejido)

        self.treeview.connect("button-press-event", self.rightClick)
        self.treeview.connect("cursor-changed",self.calcular_turno_siguiente)

        self.button_historial_turnos.connect("clicked",self.function_historial_turnos)
        self.button_limpieza_turnos.connect("clicked",self.function_limpieza_turnos)

        self.cellrenderer_toggle.connect("toggled", self.cell_toggled,self.liststore_turnos)

    def menu(self, widget, boton, pos, tiempo):
        """Al realizar click derecho en la lista muestro un menu.
            Dependiendo del estado del turno : agrega,elimina o modifica"""
        (model, iter) = self.treeselection.get_selected()
        paciente=self.liststore_turnos.get_value(iter,2)
        menu = gtk.Menu()
        add=gtk.Image()
        delete=gtk.Image()
        modific=gtk.Image()
        add.set_from_file("Recursos/add2.png")
        delete.set_from_file("Recursos/remove2.png")
        modific.set_from_file("Recursos/modific.png")
        agregar = gtk.ImageMenuItem()
        eliminar = gtk.ImageMenuItem()
        actualizar = gtk.ImageMenuItem()
        agregar.set_image(add)
        eliminar.set_image(delete)
        actualizar.set_image(modific)
        agregar.set_label("Agregar Turno")
        eliminar.set_label("Eliminar Turno")
        actualizar.set_label("Modificar Turno")
        menu.append(agregar)
        menu.append(eliminar)
        menu.append(actualizar)
        agregar.connect("activate", self.function_agregar_turno,iter)
        eliminar.connect("activate", self.function_eliminar_turno,iter)
        actualizar.connect("activate", self.function_modificar_turno,iter)

        if paciente != "":#ya hay un turno en la fila seleccionada,no debe poder agregar
            agregar.set_sensitive(False)
        elif paciente == "": #fila seleccionada vacia,no debe poder eliminar o modificar
            eliminar.set_sensitive(False)
            actualizar.set_sensitive(False)
        menu.show_all()
        menu.popup(None, None,None, boton, tiempo)


    def cell_color(self):
        """Le doy color de fondo a los cellrender"""
        celdas=("cellrenderertext1","cellrenderertext2","cellrenderertext3","cellrenderertoggle1")
        columnas=("treeviewcolumn1","treeviewcolumn2","treeviewcolumn3","treeviewcolumn4")
        for x,columna in enumerate(columnas):
            if columna == "treeviewcolumn4":#el cell cleckbutton
                self.glade.get_object(columna).set_attributes(self.glade.get_object(celdas[x]),
                                                            active=x+1,
                                                            cell_background_set=5)
            else:
                self.glade.get_object(columna).set_attributes(self.glade.get_object(celdas[x]),
                                                            text=x+1,
                                                            cell_background_set=5)
            self.glade.get_object(celdas[x]).set_property('cell-background','#E0E0E0')
