#!/usr/bin/env python
# -*- coding: utf-8 -*-


import gtk
import pygtk

pygtk.require('2.0')

from Controladores.Pacientes.Agregar.agregar_paciente import Nuevo_Paciente
from Controladores.Pacientes.Editar_pacientes.editar_pacientes import Editar_Pacientes

class SectorPaciente():

    def function_agregar_paciente(self,widget):
        """Ventana para agregar paciente"""
        Nuevo_Paciente()

    def function_editar_pacientes(self,widget):
        """Ventana para editar pacientes(eliminar,modificar)"""
        Editar_Pacientes(self.calendar)

    def __init__(self):

        self.button_agregar_paciente=self.glade.get_object("agregar_paciente")
        self.button_editar_pacientes=self.glade.get_object("editar_pacientes")


        self.button_agregar_paciente.connect("clicked",self.function_agregar_paciente)
        self.button_editar_pacientes.connect("clicked",self.function_editar_pacientes)