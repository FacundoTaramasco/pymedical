#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk
import datetime

from Modelos.Model_Paciente import Todos_Pacientes

from Controladores.Utilidades import Acomodar_Fecha
from Controladores.Utilidades import Fecha_Para_Calendar

from Controladores.Pacientes.Eliminar.eliminar_paciente import Eliminar_Paciente
from Controladores.Pacientes.Modificar.modificar_paciente import Modificar_Paciente



class Editar_Pacientes():

    def eliminar_paciente(self,widget):
        Values_from_edit=(self.id_paciente,self.liststore_pacientes)
        Eliminar_Paciente(Values_from_edit)

    def modificar_paciente(self,widget):
        Values_from_edit=(self.id_paciente,self.liststore_pacientes)
        Modificar_Paciente(Values_from_edit)

    def target(self,iter):
        self.id_paciente=self.liststore_pacientes.get_value(iter,0)

    def rightClick(self,widget,event):
        click = event.button
        pos = (event.x, event.y)
        tiempo = event.time
        if click == 3:
            self.menu(widget, click, pos, tiempo)

    def delete_event(self,widget,event):
        self.target_calendar()
        self.window.destroy()

    def cerrar(self,widget):
        self.target_calendar()
        self.window.destroy()

    def __init__(self,calendar_main):

        self.calendar_main=calendar_main

        archivo="Vistas/Pacientes/editar_pacientes.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)

        self.window=glade.get_object("dialog1")
        self.button_cerrar=glade.get_object("button_cerrar")
        self.liststore_pacientes=glade.get_object("liststore1")
        self.treeview=glade.get_object("treeview1")

        self.treeselection = self.treeview.get_selection()

        self.agregando_pacientes()

        self.treeview.add_events(gtk.gdk.BUTTON2_MASK)
        self.treeview.connect("button-press-event", self.rightClick)

        self.button_cerrar.connect("clicked",self.cerrar)
        self.window.connect("delete_event",self.delete_event)

    def agregando_pacientes(self):
        self.liststore_pacientes.clear()
        lista=Todos_Pacientes()
        for Pac in lista:
            self.liststore_pacientes.append([Pac.getId(),
                                            Pac.getNomyApe(),
                                            Pac.getDni(),
                                            Acomodar_Fecha(Pac.getFechanac()),
                                            Pac.getLocalidad(),
                                            Pac.getCodigoPostal(),
                                            Pac.getDomicilio(),
                                            Pac.getTelefono() ] )

    def menu(self, widget, boton, pos, tiempo):
        (model, iter) = self.treeselection.get_selected()
        menu = gtk.Menu()
        delete=gtk.Image()
        modific=gtk.Image()
        eliminar = gtk.ImageMenuItem()
        actualizar = gtk.ImageMenuItem()
        delete.set_from_file("Recursos/remove2.png")
        modific.set_from_file("Recursos/modific.png")
        eliminar.set_label("Eliminar Paciente")
        eliminar.set_image(delete)
        actualizar.set_label("Modificar Paciente")
        actualizar.set_image(modific)
        menu.append(eliminar)
        menu.append(actualizar)
        eliminar.connect("activate", self.eliminar_paciente)
        actualizar.connect("activate", self.modificar_paciente)
        if iter != None:
            self.target(iter)
            menu.show_all()
            menu.popup(None, None,None, boton, tiempo)

    def target_calendar(self):
        today=datetime.datetime.now() #fecha actual
        dia,mes,anio=today.day,today.month-1,today.year
        self.calendar_main.select_month(mes,anio)
        self.calendar_main.select_day(dia)
