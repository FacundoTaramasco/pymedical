#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk

from Modelos.Model_Paciente import Paciente
from Modelos.Model_Paciente import Todos_Pacientes
from Modelos.Model_Paciente import Consulta_Paciente
from Modelos.Model_Paciente import Modificacion_Paciente
from Modelos.Model_Paciente import ConsultaEspecificaPAC

from Controladores.Utilidades import es_int
from Controladores.Utilidades import Acomodar_Fecha
from Controladores.Utilidades import Acomodar_dia_mes
from Controladores.Utilidades import caracteres_validos
from Controladores.Utilidades import caracteres_validos_telefono


class Modificar_Paciente():

    def modificar(self,widget):
        nomyape=self.entry_nomyape.get_text()
        dni=self.entry_dni.get_text()
        fechanac=self.entry_nac.get_text()
        fechanac=Acomodar_Fecha(fechanac)
        localidad=self.entry_loc.get_text()
        cp=self.entry_cp.get_text()
        domicilio=self.entry_dom.get_text()
        telefono=self.entry_tel.get_text()
        Pac=Paciente()
        Pac.setId(self.id_pac)
        Pac.setNomyApe(nomyape)
        Pac.setDni(dni)
        Pac.setFechanac(fechanac)
        Pac.setLocalidad(localidad)
        Pac.setCodigoPostal(cp)
        Pac.setDomicilio(domicilio)
        Pac.setTelefono(telefono)
        #Modifico los datos del paciente en la bd
        Modificacion_Paciente(Pac)
        #Actualizo el liststore de editar-pacientes.py
        self.liststore_pac_edit.clear()
        lista=Todos_Pacientes()
        for Pac in lista:
            self.liststore_pac_edit.append([Pac.getId(),
                                        Pac.getNomyApe(),
                                        Pac.getDni(),
                                        Acomodar_Fecha(Pac.getFechanac()),
                                        Pac.getLocalidad(),
                                        Pac.getCodigoPostal(),
                                        Pac.getDomicilio(),
                                        Pac.getTelefono() ] )

        self.window.destroy()

    def validar_nomyape(self,widget):
        self.nomyape_ok=False
        nomyape=self.entry_nomyape.get_text()
        if caracteres_validos(nomyape) and nomyape != "":
            if (ConsultaEspecificaPAC("id","nomyape",nomyape) == None) or\
            (ConsultaEspecificaPAC("id","nomyape",nomyape) == self.id_pac):
                self.entry_nomyape.set_icon_from_stock(1,gtk.STOCK_OK)
                self.entry_nomyape.set_property("secondary-icon-tooltip-text",
                                                "")
                self.nomyape_ok=True
            else:
                self.entry_nomyape.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
                self.entry_nomyape.set_property("secondary-icon-tooltip-text",
                                                "Paciente existente")
        else:
            self.entry_nomyape.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_nomyape.set_property("secondary-icon-tooltip-text",
                                            "Incorrecto")

    def validar_dni(self,widget):
        self.dni_ok=False
        dni=self.entry_dni.get_text()
        if es_int(dni) and dni != "":
            if (ConsultaEspecificaPAC("id","dni",dni) == None) or\
            (ConsultaEspecificaPAC("id","dni",dni) == self.id_pac):
                self.entry_dni.set_icon_from_stock(1,gtk.STOCK_OK)
                self.entry_dni.set_property("secondary-icon-tooltip-text","")
                self.dni_ok=True
            else:
                self.entry_dni.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
                self.entry_dni.set_property("secondary-icon-tooltip-text",
                                            "DNI existente")
        else:
            self.entry_dni.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_dni.set_property("secondary-icon-tooltip-text",
                                        "Incorrecto")

    def mostrar_calendario(self,widget,event):
        self.calendar.show()

    def ocultar_calendario(self,widget,event):
        self.calendar.hide()

    def dia_elejido(self,widget):
        self.nac_ok=True
        anio,mes,dia = self.calendar.get_date()
        anio=str(anio)
        dia,mes=Acomodar_dia_mes(dia,mes)
        self.entry_nac.set_text(dia+"-"+mes+"-"+anio  )
        self.entry_nac.set_icon_from_stock(1,gtk.STOCK_OK)

    def validar_localidad(self,widget):
        self.loc_ok=False
        localidad=self.entry_loc.get_text()
        if caracteres_validos(localidad) and localidad != "":
            self.entry_loc.set_icon_from_stock(1,gtk.STOCK_OK)
            self.entry_loc.set_property("secondary-icon-tooltip-text","")
            self.loc_ok=True
        else:
            self.entry_loc.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_loc.set_property("secondary-icon-tooltip-text",
                                        "Incorrecto")

    def validar_codigo_postal(self,widget):
        self.cp_ok=False
        codigo_postal=self.entry_cp.get_text()
        if es_int(codigo_postal) and codigo_postal != "":
            self.entry_cp.set_icon_from_stock(1,gtk.STOCK_OK)
            self.entry_cp.set_property("secondary-icon-tooltip-text","")
            self.cp_ok=True
        else:
            self.entry_cp.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_cp.set_property("secondary-icon-tooltip-text",
                                        "Incorrecto")

    def validar_domicilio(self,widget):
        self.dom_ok=False
        domicilio=self.entry_dom.get_text()
        if caracteres_validos(domicilio) and domicilio != "":
            self.entry_dom.set_icon_from_stock(1,gtk.STOCK_OK)
            self.entry_dom.set_property("secondary-icon-tooltip-text","")
            self.dom_ok=True
        else:
            self.entry_dom.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_dom.set_property("secondary-icon-tooltip-text",
                                        "Incorrecto")

    def validar_telefono(self,widget):
        self.tel_ok=False
        telefono=self.entry_tel.get_text()
        if caracteres_validos_telefono(telefono) and telefono != "":
            self.entry_tel.set_icon_from_stock(1,gtk.STOCK_OK)
            self.entry_tel.set_property("secondary-icon-tooltip-text","")
            self.tel_ok=True
        else:
            self.entry_tel.set_icon_from_stock(1,gtk.STOCK_DIALOG_ERROR)
            self.entry_tel.set_property("secondary-icon-tooltip-text",
                                        "Incorrecto")

    def desbloquear_boton_aceptar(self,widget):
        if self.nomyape_ok and self.dni_ok and self.loc_ok and \
        self.cp_ok and self.dom_ok and self.tel_ok:
            self.button_aceptar.set_sensitive(True)
        else:
            self.button_aceptar.set_sensitive(False)

    def delete_event(self,widget,event):
        self.window.destroy()

    def cerrar(self,widget):
        self.window.destroy()

    def __init__(self,Values_from_edit):
        #Values_from_edit contiene una lista con un id
        #y un liststore (ambos son de editar_pacientes.py)
        self.id_pac=Values_from_edit[0]
        self.liststore_pac_edit=Values_from_edit[1]

        archivo="Vistas/Pacientes/modificar_paciente.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)

        self.window=glade.get_object("dialog1")
        self.button_aceptar=glade.get_object("button_aceptar")
        self.button_cancelar=glade.get_object("button_cancelar")

        self.calendar=glade.get_object("calendar1")

        self.entry_nomyape=glade.get_object("entry_nomyape")
        self.entry_dni=glade.get_object("entry_dni")
        self.entry_nac=glade.get_object("entry_nac")
        self.entry_loc=glade.get_object("entry_loc")
        self.entry_cp=glade.get_object("entry_cp")
        self.entry_dom=glade.get_object("entry_dom")
        self.entry_tel=glade.get_object("entry_tel")

        (self.nomyape_ok,
        self.dni_ok,
        self.loc_ok,
        self.cp_ok,
        self.dom_ok,
        self.tel_ok)=(True,True,True,True,True,True)

        Pac_Actual=Consulta_Paciente(self.id_pac)
        self.mostrar_datos(Pac_Actual)

        self.button_aceptar.connect("clicked",self.modificar)
        self.button_cancelar.connect("clicked",self.cerrar)
        self.window.connect("delete_event",self.delete_event)

        self.entry_nomyape.connect("changed",self.validar_nomyape)
        self.entry_dni.connect("changed",self.validar_dni)
        self.entry_loc.connect("changed",self.validar_localidad)
        self.entry_cp.connect("changed",self.validar_codigo_postal)
        self.entry_dom.connect("changed",self.validar_domicilio)
        self.entry_tel.connect("changed",self.validar_telefono)

        self.calendar.connect("day-selected",self.dia_elejido)

        self.entry_nomyape.connect("focus-in-event",self.ocultar_calendario)
        self.entry_dni.connect("focus-in-event",self.ocultar_calendario)
        self.entry_loc.connect("focus-in-event",self.ocultar_calendario)
        self.entry_cp.connect("focus-in-event",self.ocultar_calendario)
        self.entry_dom.connect("focus-in-event",self.ocultar_calendario)
        self.entry_tel.connect("focus-in-event",self.ocultar_calendario)

        self.entry_nomyape.connect("changed",self.desbloquear_boton_aceptar)
        self.entry_dni.connect("changed",self.desbloquear_boton_aceptar)
        self.entry_nac.connect("changed",self.desbloquear_boton_aceptar)
        self.entry_loc.connect("changed",self.desbloquear_boton_aceptar)
        self.entry_cp.connect("changed",self.desbloquear_boton_aceptar)
        self.entry_dom.connect("changed",self.desbloquear_boton_aceptar)
        self.entry_tel.connect("changed",self.desbloquear_boton_aceptar)

        self.entry_nac.connect("focus-in-event",self.mostrar_calendario)

    def mostrar_datos(self,Pac_Actual):
        self.entry_nomyape.set_text(Pac_Actual.getNomyApe())
        self.entry_dni.set_text(str(Pac_Actual.getDni()))
        self.entry_nac.set_text(Acomodar_Fecha(Pac_Actual.getFechanac()))
        self.entry_loc.set_text(Pac_Actual.getLocalidad() )
        self.entry_cp.set_text(str(Pac_Actual.getCodigoPostal()))
        self.entry_dom.set_text(Pac_Actual.getDomicilio())
        self.entry_tel.set_text(Pac_Actual.getTelefono())
        self.nombre_paciente=self.entry_nomyape.get_text()
