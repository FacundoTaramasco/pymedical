#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk

from Modelos.Model_Paciente import Baja_Paciente
from Modelos.Model_Paciente import Todos_Pacientes
from Modelos.Model_Paciente import Consulta_Paciente

from Modelos.Model_Turno import TodosTurnosPacMed

from Controladores.Utilidades import Acomodar_Fecha

class Eliminar_Paciente():

    def borrar_paciente(self,widget):
        #Doy de baja el paciente en la bd
        Baja_Paciente(self.id_pac)
        #Actualizo el liststore de editar-pacientes.py
        self.liststore_pac_edit.clear()
        lista=Todos_Pacientes()
        for Pac in lista:
            self.liststore_pac_edit.append([Pac.getId(),
                                        Pac.getNomyApe(),
                                        Pac.getDni(),
                                        Acomodar_Fecha(Pac.getFechanac()),
                                        Pac.getLocalidad(),
                                        Pac.getCodigoPostal(),
                                        Pac.getDomicilio(),
                                        Pac.getTelefono()])
        self.window.destroy()

    def delete_event(self,widget,event):
        self.window.destroy()

    def cerrar(self,widget):
        self.window.destroy()

    def __init__(self,Values_from_edit):
        #Values_from_edit contiene una lista con un id
        #y un liststore (ambos son de editar_pacientes.py)

        self.id_pac=Values_from_edit[0]
        self.liststore_pac_edit=Values_from_edit[1]

        archivo="Vistas/Pacientes/eliminar_paciente.glade"
        glade=gtk.Builder()
        glade.add_from_file(archivo)

        self.window=glade.get_object("dialog1")
        self.liststore_turnos=glade.get_object("liststore3")
        self.button_cancelar=glade.get_object("button_cancelar")
        self.button_aceptar=glade.get_object("button_aceptar")
        self.entry_nomyape=glade.get_object("entry_nomyape")
        self.entry_dni=glade.get_object("entry_dni")
        self.entry_nac=glade.get_object("entry_nac")
        self.entry_loc=glade.get_object("entry_loc")
        self.entry_cp=glade.get_object("entry_cp")
        self.entry_dom=glade.get_object("entry_dom")
        self.entry_tel=glade.get_object("entry_tel")

        columna_toggle=glade.get_object("treeviewcolumn6")
        self.cellrenderer_toggle = gtk.CellRendererToggle()
        columna_toggle.pack_start(self.cellrenderer_toggle, True)
        columna_toggle.add_attribute(self.cellrenderer_toggle, "active", 5)

        Pac_Actual=Consulta_Paciente(self.id_pac)
        self.mostrar_datos(Pac_Actual)

        lista=TodosTurnosPacMed("id_paciente",self.id_pac)
        for tur in lista:
            self.liststore_turnos.append([ tur.getHora(),
                                Acomodar_Fecha(tur.getFecha()),
                                tur.getPaciente(),
                                tur.getMedico(),
                                tur.getCausa(),
                                tur.getEstado()])

        self.button_cancelar.connect("clicked",self.cerrar)
        self.window.connect("delete_event",self.delete_event)
        self.button_aceptar.connect("clicked",self.borrar_paciente)

    def mostrar_datos(self,Pac_Actual):
        self.entry_nomyape.set_text(Pac_Actual.getNomyApe() )
        self.entry_dni.set_text(str(Pac_Actual.getDni()))
        self.entry_nac.set_text(Acomodar_Fecha(Pac_Actual.getFechanac()))
        self.entry_loc.set_text(Pac_Actual.getLocalidad())
        self.entry_cp.set_text(str(Pac_Actual.getCodigoPostal()))
        self.entry_dom.set_text(Pac_Actual.getDomicilio())
        self.entry_tel.set_text(Pac_Actual.getTelefono())
