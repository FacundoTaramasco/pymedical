#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3

from Modelos.Model_BD import Leer_Ruta_BBDD

class Turno():
    """Esquema de tabla turnos"""
    def __init__(self):
        self.__id=None
        self.__hora=""
        self.__fecha=""
        self.id_pac=None
        self.id_med=None
        self.__causa=""
        self.__estado=False

        #auxiliares
        #-------------------#
        self.__paciente=""
        self.__medico=""

    def getId(self):
        return self.__id
    def setId(self,valor):
        self.__id=valor

    def getHora(self):
        return self.__hora
    def setHora(self,valor):
        self.__hora=valor

    def getFecha(self):
        return self.__fecha

    def setFecha(self,valor):
        self.__fecha=valor

    def getIdPac(self):
        return self.id_pac
    def setIdPac(self,valor):
        self.id_pac=valor

    def getIdMed(self):
        return self.id_med
    def setIdMed(self,valor):
        self.id_med=valor

    def getCausa(self):
        return self.__causa
    def setCausa(self,valor):
        self.__causa=valor

    def getEstado(self):
        return self.__estado
    def setEstado(self,valor):
        self.__estado=valor

    #auxiliares
    #-------------------------#
    def getPaciente(self):
        return self.__paciente
    def setPaciente(self,valor):
        self.__paciente=valor

    def getMedico(self):
        return self.__medico
    def setMedico(self,valor):
        self.__medico=valor

def wtftestquery():
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""SELECT TURN.id,TURN.hora,TURN.fecha,TURN.causa,TURN.estado,
                    PAC.nomyape,
                    MED.nomyape
                    FROM turnos TURN
                    INNER JOIN  pacientes PAC ON PAC.id == TURN.id_paciente
                    INNER JOIN  medicos MED ON  MED.ID == TURN.id_medico
                    WHERE (TURN.id_medico = 6)
                    AND
                    (strftime('%w',TURN.fecha) = '2')
                    ORDER BY TURN.fecha ASC, TURN.hora ASC """)
    for tupla in cursor.fetchall():
        print tupla
    cursor.close()
    bbdd.close()


def Turnos_de_Mes(fecha_inicial,fecha_final,id_medico):
    """Selecciono las fechas unicas de turnos que esten entre un determinado mes
    dependiendo del medico.Utilizado para hacer una marca en gtk calendar"""
    lista_dias=[]
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""SELECT DISTINCT fecha
                    FROM turnos
                    WHERE id_medico=?
                    AND
                    fecha BETWEEN ? AND ? """,
                    (id_medico,fecha_inicial,fecha_final))
    for tupla in cursor.fetchall():
        #print tupla[0][8:] #tupla[0][8:] == dia
        lista_dias.append(tupla[0][8:])
    cursor.close()
    bbdd.close()
    return lista_dias

def Consulta_Turno(id_turn):
    """consulta de un turno a partir de su id"""
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""SELECT TURN.id,TURN.hora,TURN.fecha,TURN.causa,TURN.estado,
                PAC.nomyape,
                MED.nomyape
                FROM turnos TURN
                INNER JOIN  pacientes PAC ON PAC.id == TURN.id_paciente
                INNER JOIN  medicos MED ON  MED.ID == TURN.id_medico
                WHERE TURN.id = ? """,(id_turn,) )
    values=cursor.fetchone()
    cursor.close()
    bbdd.close()
    Tur=Turno()
    Tur.setId(values[0])
    Tur.setHora(values[1])
    Tur.setFecha(values[2])
    Tur.setCausa(values[3])
    Tur.setEstado(values[4])
    Tur.setPaciente(values[5])
    Tur.setMedico(values[6])
    return Tur

def Busqueda_Turnos(search,tipo):
    """busqueda de turnos segun el nombre y apellido ('searh')de un medico
    o paciente ('tipo')"""
    lista=[]
    #tipo --> medico o paciente
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""SELECT TURN.id,TURN.hora,TURN.fecha,TURN.causa,TURN.estado,
                PAC.nomyape,
                MED.nomyape
                FROM turnos TURN
                INNER JOIN  pacientes PAC ON PAC.id == TURN.id_paciente
                INNER JOIN  medicos MED ON  MED.ID == TURN.id_medico
                WHERE """+tipo+""".nomyape
                LIKE '%"""+search+
                """'
                AND
                (TURN.fecha <> 'NULL' )
                ORDER BY TURN.fecha ASC,TURN.hora ASC """)
    for tupla in cursor.fetchall():
        Tur=Turno()
        Tur.setId(tupla[0])
        Tur.setHora(tupla[1])
        Tur.setFecha(tupla[2])
        Tur.setCausa(tupla[3])
        Tur.setEstado(tupla[4])
        Tur.setPaciente(tupla[5])
        Tur.setMedico(tupla[6])
        lista.append(Tur)
    cursor.close()
    bbdd.close()
    return lista




def Turnos_Especificos(Turn):
    """Funcion que retorna todos los turnos de una dia,especificando el dia
    y el medico"""
    lista=[]
    bbdd=sqlite3.connect(Leer_Ruta_BBDD())
    cursor=bbdd.cursor()

    cursor.execute("""SELECT TURN.id,TURN.hora,TURN.fecha,TURN.causa,TURN.estado,
                PAC.nomyape,
                MED.nomyape
                FROM turnos TURN
                INNER JOIN  pacientes PAC ON PAC.id == TURN.id_paciente
                INNER JOIN  medicos MED ON  MED.ID == TURN.id_medico
                WHERE ( TURN.id_medico = ? )
                AND (TURN.fecha = ? )
                ORDER BY TURN.fecha ASC,TURN.hora ASC""",
                (Turn.getIdMed(),Turn.getFecha()) )
    for tupla in cursor.fetchall():
        Tur=Turno()
        Tur.setId(tupla[0])
        Tur.setHora(tupla[1])
        Tur.setFecha(tupla[2])
        Tur.setCausa(tupla[3])
        Tur.setEstado(tupla[4])
        Tur.setPaciente(tupla[5])
        Tur.setMedico(tupla[6])
        lista.append(Tur)
    cursor.close()
    bbdd.close()
    return lista


def TodosTurnosPacMed(col,val_id):
    """col : espicifica si se trata de un medico o un paciente (id_medico/id_paciente)
        id : id del medico o del paciente
    una lista con todos los turnos de un paciente o medico,ordenados de forma
    ascendente por fecha y hora"""
    lista=[]
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""SELECT TURN.id,TURN.hora,TURN.fecha,TURN.causa,TURN.estado,
                PAC.nomyape,
                MED.nomyape
                FROM turnos TURN
                INNER JOIN  pacientes PAC ON PAC.id == TURN.id_paciente
                INNER JOIN  medicos MED ON  MED.ID == TURN.id_medico
                WHERE
                (TURN."""+col+""" = ?)
                AND
                (TURN.fecha <> 'NULL' )
                ORDER BY TURN.fecha ASC,TURN.hora ASC """,(val_id,)  )

    for tupla in cursor.fetchall():
        Tur=Turno()
        Tur.setId(tupla[0])
        Tur.setHora(tupla[1])
        Tur.setFecha(tupla[2])
        Tur.setCausa(tupla[3])
        Tur.setEstado(tupla[4])
        Tur.setPaciente(tupla[5])
        Tur.setMedico(tupla[6])
        lista.append(Tur)
    cursor.close()
    bbdd.close()
    return lista

def TurnosDiaSemana(id_medico,dia):
    #STRFTIME('%w', variablefecha)
    #funcion que devuelve 0,1,2,...6 dependiendo el dia que sea
    #comenzando con domingo = 0
    """Funcion que retorna una lista con los turnos dependiendo del medico
    y del dia de la semana(lunes,martes,etc)"""
    lista=[]
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""SELECT TURN.id,TURN.hora,TURN.fecha,TURN.causa,TURN.estado,
                    PAC.nomyape,
                    MED.nomyape
                    FROM turnos TURN
                    INNER JOIN pacientes PAC ON PAC.id == TURN.id_paciente
                    INNER JOIN medicos MED ON  MED.ID == TURN.id_medico
                    WHERE (TURN.id_medico = ?)
                    AND
                    (STRFTIME('%w', TURN.fecha) = ?)
                    ORDER BY TURN.fecha ASC, TURN.hora ASC """,
                    (id_medico,dia) )
    for tupla in cursor.fetchall():
        Tur=Turno()
        Tur.setId(tupla[0])
        Tur.setHora(tupla[1])
        Tur.setFecha(tupla[2])
        Tur.setCausa(tupla[3])
        Tur.setEstado(tupla[4])
        Tur.setPaciente(tupla[5])
        Tur.setMedico(tupla[6])
        lista.append(Tur)
    cursor.close()
    bbdd.close()
    return lista


def ExistenciaFechaNULL():
    """Si existe por lo menos un turno con fecha == 'NULL' retorno id,
    sino retorno None"""
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""SELECT id FROM turnos
                    WHERE fecha = 'NULL' LIMIT 1 """)
    val=cursor.fetchone()
    cursor.close()
    bbdd.close()
    if val : return val[0]
    else : return None

def Limpiar_Turnos(propiedad,paciente,hora_inicial,hora_final,
                    fecha_inicial,fecha_final):
    """Elimina turnos de un paciente,para eso se especifica : fecha inicial,final
    hora inicial y hora final"""
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    #print"lista completa"
    #cursor.execute("SELECT * FROM turnos WHERE paciente=?  ORDER BY fecha ASC,hora ASC ",(paciente,) )
    #for tupla in cursor.fetchall():
        #print tupla[0],tupla[1],tupla[2],tupla[3],tupla[4],tupla[5],tupla[6]
    #print "-"*60
    if propiedad=="todos" :
        #print"borrando todo"
        cursor.execute(""" DELETE from turnos WHERE paciente =?""",(paciente,) )
    elif propiedad=="seleccion" :
        #print"borrando seleccion"
        #print "hora inicial : %s | hora final : %s | fecha inicial : %s | fecha final : %s" % (hora_inicial,hora_final,fecha_inicial,fecha_final)
        #cursor.execute("SELECT * FROM turnos WHERE paciente=?    AND    fecha BETWEEN ? AND ?    AND    hora BETWEEN ? AND ?       ORDER BY fecha ASC,hora ASC " ,(paciente,fecha_inicial,fecha_final,hora_inicial,hora_final) )
        #for tupla in cursor.fetchall():
            #print tupla[0],tupla[1],tupla[2],tupla[3],tupla[4],tupla[5],tupla[6]
        cursor.execute("""DELETE FROM turnos
                            WHERE paciente=?
                            AND
                            fecha BETWEEN ? AND ?
                            AND
                            hora BETWEEN ? AND ? """ ,
                            (paciente,
                            fecha_inicial,fecha_final,
                            hora_inicial,hora_final) )
    bbdd.commit()
    cursor.close()
    bbdd.close()

def Lista_turnos_seleccion(col,val_id,hora_inicial,hora_final,
                            fecha_inicial,fecha_final):
    """Utilizado en limpieza de turnos,una lista con todos los turnos
    a eliminar,por medico o por paciente"""
    lista=[]
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute(    """SELECT TURN.id,TURN.hora,TURN.fecha,TURN.causa,TURN.estado,
                PAC.nomyape,
                MED.nomyape
                FROM turnos TURN
                INNER JOIN  pacientes PAC ON PAC.id == TURN.id_paciente
                INNER JOIN  medicos MED ON  MED.ID == TURN.id_medico
                WHERE (TURN."""+col+""" = ?)
                AND
                (TURN.fecha BETWEEN ? AND ?)
                AND
                (TURN.hora BETWEEN ? AND ?)
                ORDER BY TURN.fecha ASC,TURN.hora ASC """ ,
                (val_id,
                fecha_inicial,fecha_final,
                hora_inicial,hora_final) )
    for tupla in cursor.fetchall():
        Tur=Turno()
        Tur.setId(tupla[0])
        Tur.setHora(tupla[1])
        Tur.setFecha(tupla[2])
        Tur.setCausa(tupla[3])
        Tur.setEstado(tupla[4])
        Tur.setPaciente(tupla[5])
        Tur.setMedico(tupla[6])
        lista.append(Tur)
        #print tupla[0],tupla[1],tupla[2],tupla[3],tupla[4],tupla[5],tupla[6]
    cursor.close()
    bbdd.close()
    return lista

###############################ABM############################################

def Alta_Turno(Tur):
    """Alta de un turno en la bd"""
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute(""" INSERT INTO turnos
                                    (hora,
                                    fecha,
                                    id_paciente,
                                    id_medico,
                                    causa,
                                    estado)
                                    VALUES(?,?,?,?,?,?)""",
                                    (Tur.getHora(),
                                    Tur.getFecha(),
                                    Tur.getIdPac(),
                                    Tur.getIdMed(),
                                    Tur.getCausa(),
                                    Tur.getEstado() ) )
    bbdd.commit()
    cursor.close()
    bbdd.close()


def Baja_Turno(id_turn):
    """Baja de un turno en la bd"""
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute(""" DELETE from turnos WHERE id =?""",(id_turn,) )
    bbdd.commit()
    cursor.close()
    bbdd.close()

def Modificacion_Turno(Tur):
    """Modificacion de un turno en la bd"""
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""UPDATE turnos SET
                            hora =?,
                            fecha=?,
                            id_paciente=?,
                            id_medico=?,
                            causa=?
                            WHERE id =?""",
                            (Tur.getHora(),
                            Tur.getFecha(),
                            Tur.getIdPac(),
                            Tur.getIdMed(),
                            Tur.getCausa(),
                            Tur.getId()))
    bbdd.commit()
    cursor.close()
    bbdd.close()


def Actualizar_Estado(Tur):
    """Modificacion del estado de un turno (True or False)"""
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""UPDATE turnos SET estado=?
                    WHERE id=? """,
                    (Tur.getEstado(),Tur.getId() ) )
    bbdd.commit()
    cursor.close()
    bbdd.close()
##############################################################################


def ModFechaNULL(id_medico,diasemana):
    """Modifica fecha a valor NULL dependiendo del medico y del dia de la semana.
    Ej. si un medico deja de trabajar un dia de la semana y existian turnos
    en el mismo"""
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""UPDATE turnos SET
                            fecha = 'NULL'
                            WHERE (id_medico = ?)
                            AND
                            (STRFTIME('%w',fecha) = ?)""",
                            (id_medico,diasemana))
    bbdd.commit()
    cursor.close()
    bbdd.close()

def TodosTurnosNULL():
    lista=[]
    bbdd=sqlite3.connect(Leer_Ruta_BBDD())
    cursor=bbdd.cursor()
    cursor.execute("""SELECT TURN.id,TURN.hora,TURN.fecha,TURN.causa,TURN.estado,
                PAC.nomyape,
                MED.nomyape
                FROM turnos TURN
                INNER JOIN  pacientes PAC ON PAC.id == TURN.id_paciente
                INNER JOIN  medicos MED ON  MED.ID == TURN.id_medico
                WHERE TURN.fecha = 'NULL'
                """ )
                #ORDER BY TURN.fecha ASC,TURN.hora ASC
    for tupla in cursor.fetchall():
        Tur=Turno()
        Tur.setId(tupla[0])
        Tur.setHora(tupla[1])
        Tur.setFecha(tupla[2])
        Tur.setCausa(tupla[3])
        Tur.setEstado(tupla[4])
        Tur.setPaciente(tupla[5])
        Tur.setMedico(tupla[6])
        lista.append(Tur)
    cursor.close()
    bbdd.close()
    return lista
