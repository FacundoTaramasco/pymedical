#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3

from Modelos.Model_BD import Leer_Ruta_BBDD

class Medico():
    """Esquema de tabla medicos"""
    def __init__(self):
        self.__id=None
        self.__nomyape=""
        self.__dni=0
        self.__nromat=0
        self.__telefono=""
        self.__domicilio=""
        self.__trabaja_lunes=False
        self.__trabaja_martes=False
        self.__trabaja_miercoles=False
        self.__trabaja_jueves=False
        self.__trabaja_viernes=False
        self.__trabaja_sabado=False
        self.__trabaja_domingo=False

    def getId(self):
        return self.__id
    def setId(self,valor):
        self.__id=valor

    def getNomyApe(self):
        return self.__nomyape
    def setNomyApe(self,valor):
        self.__nomyape=valor

    def getDni(self):
        return self.__dni
    def setDni(self,valor):
        self.__dni=valor

    def getNroMat(self):
        return self.__nromat
    def setNroMat(self,valor):
        self.__nromat=valor

    def getTelefono(self):
        return self.__telefono
    def setTelefono(self,valor):
        self.__telefono=valor

    def getDomicilio(self):
        return self.__domicilio
    def setDomicilio(self,valor):
        self.__domicilio=valor

    def getTrabajaLunes(self):
        return self.__trabaja_lunes
    def setTrabajaLunes(self,valor):
        self.__trabaja_lunes=valor

    def getTrabajaMartes(self):
        return self.__trabaja_martes
    def setTrabajaMartes(self,valor):
        self.__trabaja_martes=valor

    def getTrabajaMiercoles(self):
        return self.__trabaja_miercoles
    def setTrabajaMiercoles(self,valor):
        self.__trabaja_miercoles=valor

    def getTrabajaJueves(self):
        return self.__trabaja_jueves
    def setTrabajaJueves(self,valor):
        self.__trabaja_jueves=valor

    def getTrabajaViernes(self):
        return self.__trabaja_viernes
    def setTrabajaViernes(self,valor):
        self.__trabaja_viernes=valor

    def getTrabajaSabado(self):
        return self.__trabaja_sabado
    def setTrabajaSabado(self,valor):
        self.__trabaja_sabado=valor

    def getTrabajaDomingo(self):
        return self.__trabaja_domingo
    def setTrabajaDomingo(self,valor):
        self.__trabaja_domingo=valor

def ConsultaEspecificaMED(columna1,columna2, value ):
    """Consulta de una valor especifico"""
    bbdd=sqlite3.connect( Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""SELECT """+columna1+""" FROM medicos
                    WHERE """+columna2+""" =? """,(value,) )
    valor=cursor.fetchone()
    cursor.close()
    bbdd.close()
    if valor : return valor[0]
    else: return None


def Consulta_Medico(id_med):
    """Consulta de un medico a travez de su id"""
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""SELECT * FROM medicos WHERE id=?""",(id_med,) )
    values=cursor.fetchone()
    cursor.close()
    bbdd.close()
    Med=Medico()
    Med.setId(values[0])
    Med.setNomyApe(values[1])
    Med.setDni(values[2])
    Med.setNroMat(values[3])
    Med.setTelefono(values[4])
    Med.setDomicilio(values[5])
    Med.setTrabajaLunes(values[6])
    Med.setTrabajaMartes(values[7])
    Med.setTrabajaMiercoles(values[8])
    Med.setTrabajaJueves(values[9])
    Med.setTrabajaViernes(values[10])
    Med.setTrabajaSabado(values[11])
    Med.setTrabajaDomingo(values[12])
    return Med

def Todos_Medicos():
    """Lista con todos los medicos existentes"""
    lista=[]
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""SELECT * FROM medicos """)
    for tupla in cursor.fetchall():
        Med=Medico()
        Med.setId(tupla[0])
        Med.setNomyApe(tupla[1])
        Med.setDni(tupla[2])
        Med.setNroMat(tupla[3])
        Med.setTelefono(tupla[4])
        Med.setDomicilio(tupla[5])
        lista.append(Med)
    cursor.close()
    bbdd.close()
    return lista



#################################################ABM##########################
def Alta_Medico(Med):
    """Alta de medico en la bd"""
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute(""" INSERT INTO medicos (nomyape,
                                    dni,nromat,
                                    telefono,
                                    domicilio,
                                    trabaja_lunes,
                                    trabaja_martes,
                                    trabaja_miercoles,
                                    trabaja_jueves,
                                    trabaja_viernes,
                                    trabaja_sabado,
                                    trabaja_domingo)
                                    VALUES(?,?,?,?,?,?,?,?,?,?,?,?)""",
                                    (Med.getNomyApe(),
                                    Med.getDni(),
                                    Med.getNroMat(),
                                    Med.getTelefono(),
                                    Med.getDomicilio(),
                                    Med.getTrabajaLunes(),
                                    Med.getTrabajaMartes(),
                                    Med.getTrabajaMiercoles(),
                                    Med.getTrabajaJueves(),
                                    Med.getTrabajaViernes(),
                                    Med.getTrabajaSabado(),
                                    Med.getTrabajaDomingo() )
                                    )
    bbdd.commit()
    cursor.close()
    bbdd.close()

def Baja_Medico(id_med):
    """Baja de medico en la bd"""
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""DELETE from medicos WHERE id =? """,(id_med,) )
    bbdd.commit()
    cursor.close()
    bbdd.close()

def Modificacion_Medico(Med):
    """Modifiacion de medico en la bd"""
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute(""" UPDATE medicos SET nomyape=?,
                                        dni=?,
                                        nromat=?,
                                        telefono=?,
                                        domicilio=?,
                                        trabaja_lunes=?,
                                        trabaja_martes=?,
                                        trabaja_miercoles=?,
                                        trabaja_jueves=?,
                                        trabaja_viernes=?,
                                        trabaja_sabado=?,
                                        trabaja_domingo=?
                                        WHERE id =?""",
                                        (Med.getNomyApe(),
                                        Med.getDni(),
                                        Med.getNroMat(),
                                        Med.getTelefono(),
                                        Med.getDomicilio(),
                                        Med.getTrabajaLunes(),
                                        Med.getTrabajaMartes(),
                                        Med.getTrabajaMiercoles(),
                                        Med.getTrabajaJueves(),
                                        Med.getTrabajaViernes(),
                                        Med.getTrabajaSabado(),
                                        Med.getTrabajaDomingo(),
                                        Med.getId()
                                        ) )
    bbdd.commit()
    cursor.close()
    bbdd.close()

##############################################################################