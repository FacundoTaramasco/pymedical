#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3

from Modelos.Model_BD import Leer_Ruta_BBDD

class Paciente():
    """Esquema de tabla pacientes"""
    def __init__(self):
        self.__id=None
        self.__nomyape=""
        self.__dni=0
        self.__fechanac=""
        self.__localidad=""
        self.__cp=0
        self.__domicilio=""
        self.__telefono=""

    def getId(self):
        return self.__id
    def setId(self,valor):
        self.__id=valor

    def getNomyApe(self):
        return self.__nomyape
    def setNomyApe(self,valor):
        self.__nomyape=valor

    def getDni(self):
        return self.__dni
    def setDni(self,valor):
        self.__dni=valor

    def getFechanac(self):
        return self.__fechanac
    def setFechanac(self,valor):
        self.__fechanac=valor

    def getLocalidad(self):
        return self.__localidad
    def setLocalidad(self,valor):
        self.__localidad=valor

    def getCodigoPostal(self):
        return self.__cp
    def setCodigoPostal(self,valor):
        self.__cp=valor

    def getDomicilio(self):
        return self.__domicilio
    def setDomicilio(self,valor):
        self.__domicilio=valor

    def getTelefono(self):
        return self.__telefono
    def setTelefono(self,valor):
        self.__telefono=valor

def ConsultaEspecificaPAC(columna1,columna2,value):
    """Consulta de un valor especifico"""
    bbdd=sqlite3.connect( Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""SELECT """+columna1+""" FROM pacientes
                    WHERE """+columna2+""" =? """,(value, ) )
    valor=cursor.fetchone()
    cursor.close()
    bbdd.close()
    if valor : return valor[0]
    else: return None

def Consulta_Paciente(id_pac):
    """Consulta de un paciente segun su id"""
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""SELECT * FROM pacientes  WHERE id=?""",(id_pac,) )
    values=cursor.fetchone()
    cursor.close()
    bbdd.close()
    Pac=Paciente()
    Pac.setId(values[0])
    Pac.setNomyApe(values[1])
    Pac.setDni(values[2])
    Pac.setFechanac(values[3])
    Pac.setLocalidad(values[4])
    Pac.setCodigoPostal(values[5])
    Pac.setDomicilio(values[6])
    Pac.setTelefono(values[7])
    return Pac



def Todos_Pacientes():
    """lista con todos los pacientes"""
    lista=[]
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""SELECT * FROM pacientes """)
    for tupla in cursor.fetchall():
        Pac=Paciente()
        Pac.setId(tupla[0])
        Pac.setNomyApe(tupla[1])
        Pac.setDni(tupla[2])
        Pac.setFechanac(tupla[3])
        Pac.setLocalidad(tupla[4])
        Pac.setCodigoPostal(tupla[5])
        Pac.setDomicilio(tupla[6])
        Pac.setTelefono(tupla[7])
        lista.append(Pac)
    cursor.close()
    bbdd.close()
    return lista



#####################################ABM######################################
def Alta_Paciente(Pac):
    """Alta de paciente"""
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute(""" INSERT INTO pacientes (nomyape,
                                            dni,
                                            fechanac,
                                            localidad,
                                            cp,
                                            domicilio,
                                            telefono)
                                            VALUES(?,?,?,?,?,?,?)""",
                                            (Pac.getNomyApe(),
                                            Pac.getDni(),
                                            Pac.getFechanac(),
                                            Pac.getLocalidad(),
                                            Pac.getCodigoPostal(),
                                            Pac.getDomicilio(),
                                            Pac.getTelefono() ) )
    bbdd.commit()
    cursor.close()
    bbdd.close()

def Baja_Paciente(id_pac):
    """Baja de paciente"""
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("""DELETE from pacientes WHERE id =? """,(id_pac,) )
    bbdd.commit()
    cursor.close()
    bbdd.close()

def Modificacion_Paciente(Pac):
    """Modificacion de un paciente"""
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute(""" UPDATE pacientes SET nomyape=?,
                                            dni=?,
                                            fechanac=?,
                                            localidad=?,
                                            cp=?,
                                            domicilio=?,
                                            telefono=?
                                            WHERE id=?""",
                                            (Pac.getNomyApe(),
                                            Pac.getDni(),
                                            Pac.getFechanac(),
                                            Pac.getLocalidad(),
                                            Pac.getCodigoPostal(),
                                            Pac.getDomicilio(),
                                            Pac.getTelefono(),
                                            Pac.getId() ) )
    bbdd.commit()
    cursor.close()
    bbdd.close()
##############################################################################
