#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import sqlite3 as bdapi


def Crear_Nueva_BBDD():
    bbdd=sqlite3.connect(Leer_Ruta_BBDD() )
    cursor=bbdd.cursor()
    cursor.execute("DROP TABLE IF EXISTS turnos")
    cursor.execute("DROP TABLE IF EXISTS medicos")
    cursor.execute("DROP TABLE IF EXISTS pacientes")
    cursor.execute("""CREATE TABLE turnos (
                "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT ,
                "hora" TEXT,
                "fecha" DATE,
                "id_paciente" INTEGER,
                "id_medico" INTEGER,
                "causa" TEXT,
                "estado" BOOLEAN ) """)
    cursor.execute("""CREATE TABLE medicos (
                        "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                        "nomyape" TEXT,
                        "dni" NUMERIC,
                        "nromat" NUMERIC,
                        "telefono" TEXT,
                        "domicilio" TEXT,
                        "trabaja_lunes" BOOLEAN,
                        "trabaja_martes" BOOLEAN,
                        "trabaja_miercoles" BOOLEAN,
                        "trabaja_jueves" BOOLEAN,
                        "trabaja_viernes" BOOLEAN,
                        "trabaja_sabado" BOOLEAN,
                        "trabaja_domingo" BOOLEAN) """)
    cursor.execute("""CREATE TABLE pacientes (
                        "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                        "nomyape" TEXT,
                        "dni" NUMERIC,
                        "fechanac" TEXT,
                        "localidad" TEXT,
                        "cp" INTEGER,
                        "domicilio" TEXT,
                        "telefono" TEXT
                        ) """)

    cursor.execute("""CREATE TRIGGER borrar_paciente
    BEFORE DELETE
    ON pacientes
    FOR EACH ROW
    BEGIN
        DELETE FROM turnos WHERE turnos.id_paciente = OLD.id;
    END """)

    cursor.execute("""CREATE TRIGGER borrar_medico
    BEFORE DELETE
    ON medicos
    FOR EACH ROW
    BEGIN
        DELETE FROM turnos WHERE turnos.id_medico = OLD.id;
    END """)

    bbdd.commit()
    cursor.close()
    bbdd.close()

def Verificando_Conexion_BBDD():
    try:
        bbdd=bdapi.connect( Leer_Ruta_BBDD() )
        cursor=bbdd.cursor()
        cursor.execute("""SELECT COUNT (id) FROM turnos """)
        cursor.execute("""SELECT COUNT (id) FROM medicos """)
        cursor.execute("""SELECT COUNT (id) FROM pacientes """)
        cursor.close()
        bbdd.close()
        return True
    except:
        return False



def Verificando_Nueva_Ruta_BBDD(nueva_ruta):
    try:
        pass
        #bbdd=bdapi.connect(nueva_ruta+"/turnos")
        #cursor=bbdd.cursor()
        #cursor.execute("""SELECT COUNT (id) FROM turnos """)
        #cursor.execute("""SELECT COUNT (id) FROM medicos """)
        #cursor.execute("""SELECT COUNT (id) FROM pacientes """)
        #cursor.close()
        #bbdd.close()
        return True
    except:
        return False

def Leer_Ruta_BBDD():
    manejador=open("Controladores/.ruta_db.csv","r")
    manejador_csv=csv.reader(manejador)
    path=manejador_csv.next()
    manejador.close()
    return path[0]

def Guardar_Ruta_Turnos(nueva_ruta):
    ruta=[]
    ruta.append(nueva_ruta+"/turnos")
    manejador=open("Controladores/Turnos/ruta_db.csv","w")
    manejador_csv=csv.writer(manejador)
    manejador_csv.writerow(ruta)
    manejador.close()
